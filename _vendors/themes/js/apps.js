var _appsJS = (function() {

	getListJenisByMerk = function ($merkID, $curr = null) {
		$.ajax({
			url: $uri+'list_jenis',
			type: 'GET',
			data: {
				'mid': $merkID,
				'current' : $curr
			},
			success: function(res) {
				$("#ijenis").html(res);
				$("#ijenis").select2({ 
					theme: "bootstrap-5", 
					placeholder : 'Pilih Tipe Mobil',
					allowClear: true
				}).on('change', function() {
					$("input[name='jenis']").val($(this).val());
					// console.log($(this).val());
				});
			}
		});
	}

	getListMobil = function($mid = null, $jid = null, $tsewa = null, $hari = null) {
		$.ajax({
			url: $uri+'lists',
			type: 'GET',
			data: {
				'mid': $mid,
				'jid': $jid,
				'tsewa':$tsewa,
				'hari': $hari
			},
			beforeSend: function() {
				$(".lists-area").html(createPlacholderLoading(12));
			},
			success: function(res) {
				$(".lists-area").html(res);
			}
		});
	}

	createPlacholderLoading = function($jml) {
		var output = '';
    for (var count = 0; count < $jml; count++) {
        output += '<div class="col-3">';
        	output += '<div class="ph-item">';
        		output += '<div class="ph-col-12">';
        			output += '<div class="ph-picture"></div>';
        			output += '<div class="ph-row">';
        				output += '<div class="ph-col-6 big"></div>';
        				output += '<div class="ph-col-4 empty big"></div>';
        				output += '<div class="ph-col-4"></div>';
        				output += '<div class="ph-col-8 empty"></div>';
        				output += '<div class="ph-col-6"></div>';
        				output += '<div class="ph-col-6 empty"></div>';
        				output += '<div class="ph-col-12"></div>';
        			output += '</div>';
        		output += '</div>';
        	output += '</div>';
        output += '</div>';
    }
    return output;
	}

	calculateRentCost = function($inputDate, $lamaSewa) {
		var startDate = $inputDate.split("/").reverse().join("-"),
				hargaSewa = parseInt($("input[name='harga_sewa']").val()) * $lamaSewa,
				endDate = moment(startDate).add($lamaSewa, 'days').format('DD MMM YYYY');
		
		$("#ketSewa").html("<small><strong>"+moment(startDate).format('DD MMM YYYY') +" - "+endDate+"</strong></small>");
		$("#hargaSewa").html("<small><strong>"+accounting.formatNumber(hargaSewa)+"</strong></small>");
		$("#totalHarga").html("<h5> Rp. "+accounting.formatNumber(hargaSewa)+"</h5>");
	}

	showModalConfirmation = function($penyewaanID) {
		$.ajax({
			url: $uri+'konfirmasi',
			type: 'GET',
			data: {'pid' : $penyewaanID},
			beforeSend: function() {},
			success: function(res) {
				$("#modalApps .modal-title").html('Konfirmasi Pembayaran <br> <small>No. Transaksi: '+$penyewaanID+'</small>');
				$("#modalApps .modal-body").html(res);
				$('#modalApps').modal('show'); 
			}
		});
	}

	createDateTimePicker = function ($el) {
		$($el).datetimepicker({
		 	format: 'L',
		 	locale: 'id'
    });
	}

	showPassword = function($el) {
    var tipe = $(".password").attr('type');
    if(tipe == 'password') {
      $(".password").attr('type', 'text');
      $el.html('<i class="fa fa-eye-slash"></i>');
    }else{
      $(".password").attr('type', 'password');
      $el.html('<i class="fa fa-eye"></i>');
    }
  }

	return {
		init: function() {
			if($('.spinner-input').length) $(".spinner-input").inputSpinner();
			if($('.select2').length) $('.select2').select2({ theme: "bootstrap-5", placeholder : 'Pilih Merk Mobil', allowClear: true });

			$('body').on('change', '.smerk', function() {
				getListJenisByMerk($(this).val());
			});

			createDateTimePicker('#datetimepicker2'); // utk home

	    $('#dtsewa').datetimepicker({
			 	format: 'L',
			 	locale: 'id'
	    });

	    $(".spinner-input-sewa").inputSpinner({
	    	template: '<div class="input-group">' +
	    	'<button class="btn btn-decrement btn-secondary" type="button">${decrementButton}</button>'+
        '<input type="text" inputmode="decimal" style="text-align: center" class="form-control"/>' +
        '<span class="input-group-text">Hari</span>'+
        '<button style="min-width: ${buttonsWidth}" class="btn btn-increment btn-secondary" type="button">${incrementButton}</button>' +
        '</div>'
      }).on('change', function(e) {
	    	var idate = $(".dtsewa").val(),
	    			lamaSewa = $(".spinner-input-sewa").val();
	    	calculateRentCost(idate, lamaSewa);
	    });


	    $("#dtsewa").on("change.datetimepicker", function (e) {
	    	var idate = moment(e.date).format('DD/MM/YYYY'),
	    			lamaSewa = $(".spinner-input-sewa").val();
	    	calculateRentCost(idate, lamaSewa);
      });

	    $("body").on('click', '.btn-konfirmasi', function() {
	    	showModalConfirmation($(this).data('pid'));
	    });

	    $("#frmReg").validate({
	    	errorElement: 'small',
	    	onkeyup: function(element) {
	    		if (!$(element).valid()) {
	    			$(":input").not("[name="+$(element).attr('name')+"]")
        		.prop("disabled", true);
	    		}else{
	    			$(":input").not("[name="+$(element).attr('name')+"]")
        		.prop("disabled", false);
	    			$(element).valid();
	    		}
	    		
	    		// $("#frmReg").valid();
	    	},
	    	rules: {
					naleng: {
						required: true,
					},
					email: {
						required: true,
						email: true
					},
					identitas: {
						required: true,
					},
					nohp: {
						required: true,
						number:true
					},
					un: {
						required: true,
					},
					pw: {
						required: true,
					},
					filektp: {
						required: true,
						extension:"jpg|png|gif|svg"
					},
					alamat: {
						required:true,
						maxlength:20
					}
	    	},
	    	errorPlacement: function(error, element) {
			    if (element.attr("name") == "pw") {
			      error.insertAfter(".btn-outline-secondary");
			    } else {
			      error.insertAfter(element);
			    }
			  }
	    });


	    if ($checkUri) {
				window.onload = function () {
					var mid = $("select[name='merk']").val(),
							jid = $("input[name='jenis']").val(),
							tsewa = $("input[name='tanggal']").val(),
							hari = $("input[name='hari']").val();
					getListJenisByMerk(mid, jid);
					getListMobil(mid, jid, tsewa, hari);
				}
			}
		}
	}
})();

_appsJS.init();