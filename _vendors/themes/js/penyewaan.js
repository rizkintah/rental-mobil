var _penyewaanJS = (function() {

  let arrStatus = {
    1 : 'Disewa',
    2 : 'Selesai Sewa',
    3 : 'Batal',
    4 : 'Menunggu Pembayaran',
    5 : 'Pembayaran Diterima',
    6 : 'Kendaraan Siap Diambil',
    7 : 'Menunggu Verifikasi Pembayaran',
  }

  qParams = function(params) {
    params.tipe       = $("select[name='tipe']").val();
    params.kat        = $("input[name='kat']:checked").val();
    params.q          = $("input[name='q']").val();
    return params;
  }

  responseHandler = function(res) {
    return res.rows
  }

  formatNumber = function(value) {
    return accounting.formatNumber(value);
  }

  statusFormatter = function (value) {
    return arrStatus[value];
  }

  tanggalFormatter = function (value) {
    return moment(value).format('DD MMM YYYY');
  }

  detailViewFormatter = function(value, row, index) {
    var arrBtn = [];
    if (row.status_sewa == 7) {
      var arr = ["<a href='javascript:void(0);' data-token='"+JSON.stringify(row)+"' class='btn btn-sm btn-warning btnVerifikasiPembayaran'>Verifikasi</a>"];
      arrBtn.push(arr);
    }else if(row.status_sewa == 6) {
      var arr = ["<a href='javascript:void(0);' data-token='"+JSON.stringify(row)+"'  class='btn btn-sm btn-warning btnVerifikasiPenyewa'>Verifikasi Data Penyewa</a>"];
      arrBtn.push(arr);
    }else if(row.status_sewa == 1) {
      var arr = ['<a href="'+$uri+'admin/verifikasi/selesai/'+row.penyewaan_id+'?mid='+row.mobil_id+'" class="btn btn-sm btn-warning" onclick="return confirm(\'Apakah anda yakin?\');">Selesai Sewa</a>'];
      // var arr = ["<a href='"+$uri+"admin/verifikasi/selesai/"+row.penyewaan_id+"?mid"+row.mobil_id+"' class='btn btn-sm btn-warning'>Selesai Sewa</a>"];
      arrBtn.push(arr);
    }

    if ((row.status_sewa != 2) && (row.status_sewa != 3)) {
      var arr = ['<a href="'+$uri+'admin/verifikasi/batal/'+row.penyewaan_id+'?mid='+row.mobil_id+'" class="btn btn-sm btn-danger" onclick="return confirm(\'Apakah anda yakin?\');">Batal</a>'];
      arrBtn.push(arr);
    }

    

    return arrBtn.join(' ');
  }

  createTimePicker = function($element) {
    $($element).datetimepicker({
      format: 'HH:mm',
    });
  }


  showModalVerifikasiPembayaran = function(dataJSON) {
    dataJSON = JSON.parse(JSON.stringify(dataJSON));
    $html = [
        '<div class="bg-light p-1 mb-2 mt-0">',
          '<div class="row align-items-center">',
            '<div class="col-md-6 text-start">',
              '<small>No. Transaksi: <strong>'+dataJSON.penyewaan_id+'</strong></small>',
            '</div>',
            '<div class="col-md-6 text-end">',
            '<a href="'+$uri+'/admin/verifikasi/pembayaran/'+dataJSON.penyewaan_id+'" class="btn btn-sm btn-warning mt-0" onclick="return confirm(\'Apakah anda yakin?\')">Verifikasi Pembayaran</a>',
            '</div>',
          '</div>',
        '</div>',
        '<img src="'+$baseUrl+'/_files/_bukti_pembayaran/'+dataJSON.bukti_pembayaran+'" class="img-fluid" />',
    ].join('');

    $("#modalApps .modal-title").html("Verifikasi Pembayaran");
    $("#modalApps .modal-body").html($html);
    $("#modalApps").modal('show');
  }

  showModalVerifikasiPenyewa = function(dataJSON) {
    dataJSON = JSON.parse(JSON.stringify(dataJSON));
    $html = [
      '<form action="'+$uri+'admin/verifikasi/penyewa/'+dataJSON.penyewaan_id+'" method="GET" class="row g-3">', 
        '<div class="col-md-12 bg-light p-1 mb-2 mt-0">',
          '<div class="row align-items-center">',
            '<div class="col-md-6 text-start">',
              '<small>No. Transaksi: <strong>'+dataJSON.penyewaan_id+'</strong></small>',
            '</div>',
            '<div class="col-md-6 text-end">',
            '<button type="submit" class="btn btn-sm btn-warning mt-0">Verifikasi Data</button>',
            '</div>',
          '</div>',
        '</div>',
        '<div class="col-md-12">',
          '<label for="time-mulai" class="form-label fs-13 mb-1">Jam Mulai Sewa</label>',
          '<input type="text" name="jam_sewa" required readonly class="form-control form-control-sm datetimepicker-input" data-toggle="datetimepicker" data-target="#time-mulai" id="time-mulai" placeholder="HH:MM">',
        '</div>',
        // '<div class="col-md-6">',
        //   '<label for="ijs" class="form-label fs-13 mb-1">Jam Selesai Sewa</label>',
        //   '<input type="text"  required readonly class="form-control form-control-sm datetimepicker-input" data-toggle="datetimepicker" data-target="#time-selesai" id="time-selesai" placeholder="HH:MM">',
        // '</div>',
        '<div class="col-md-6">',
          '<label for="iidentitas" class="form-label fs-13 mb-1">No. Identitas</label>',
          '<input type="text" disabled class="form-control form-control-sm" id="iidentitas" placeholder="'+dataJSON.no_identitas+'">',
        '</div>',
        '<div class="col-md-6">',
          '<label for="inama" class="form-label fs-13 mb-1">Nama Penyewa</label>',
          '<input type="text" disabled class="form-control form-control-sm" id="inama" placeholder="'+dataJSON.nama_pengguna+'">',
        '</div>',
        '<div class="col-md-6">',
          '<label for="ihp" class="form-label fs-13 mb-1">No. HP</label>',
          '<input type="text" disabled class="form-control form-control-sm" id="ihp" placeholder="'+dataJSON.no_hp+'">',
        '</div>',
        '<div class="col-md-6">',
          '<label for="iemail" class="form-label fs-13 mb-1">Email Penyewa</label>',
          '<input type="text" disabled class="form-control form-control-sm" id="iemail" placeholder="'+dataJSON.email+'">',
        '</div>',
        '<div class="col-md-12">',
          '<label for="ialamat" class="form-label fs-13 mb-1">Alamat Penyewa</label>',
          '<textarea class="form-control form-control-sm" disabled>'+dataJSON.alamat+'</textarea>',
        '</div>',
        '<div class="col-md-12">',
          '<div class="accordion" id="accordionExample">',
            '<div class="accordion-item">',
              '<h4 class="accordion-header" id="headingOne">',
                '<button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">',
                'Tampilkan File Identitas',
                '</button>',
              '</h4>',

              '<div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">',
                '<div class="accordion-body">',
                  '<img src="'+$baseUrl+'/_files/_ktp/'+dataJSON.file_ktp+'" class="img-fluid" />',
                '</div>',
              '</div>',
            '</div>',
          '</div>',
        '</div>',
      '</form>',
    ].join('');

    $("#modalApps .modal-title").html("Verifikasi Data Penyewa");
    $("#modalApps .modal-body").html($html);
    createTimePicker('#time-mulai');
    createTimePicker('#time-selesai');
    $("#modalApps").modal('show');
  }

	return {
    init: function() {
      $("body").on('click', '#btnCari', function() {
        $("#tblPenyewaan").bootstrapTable('refresh');
      });

      $("body").on('click', '.btn-check', function() {
        $("#tblPenyewaan").bootstrapTable('refresh');
      });

      $("body").on('click', '.btnVerifikasiPenyewa', function () {
        showModalVerifikasiPenyewa($(this).data('token'));
      });

      $("body").on('click', '.btnVerifikasiPembayaran', function () {
        showModalVerifikasiPembayaran($(this).data('token'));
      })
    }
  }
})();


_penyewaanJS.init();


