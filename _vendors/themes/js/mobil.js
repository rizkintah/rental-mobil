var _mobilJS = (function() {
	// mobil
  btnMobil = function() {
    return {
      btnAdd: {
        text: 'Tambah Data',
        icon: 'fa-plus',
        event: function () {
          location.href = $uri+'admin/mobil/add';
        }
      }
    }
  }

  getJenisByMerkID = function($merkID, $selectedJenis = null) {
    $.ajax({
      type: "GET",
      url: $uri+'admin/mobil/lists/jenis',
      data: {
        'merk_id' : $merkID, 'curr_jenis' : $selectedJenis
      },
      beforeSend: function(){},
      success: function(res){
        $("select[name='jenis']").html(res);
      }
    });
  }

  qParams = function(params) {
    params.tipe       = $("select[name='tipe']").val();
    params.q          = $("input[name='q']").val();
    return params;
  }

  responseHandler = function(res) {
    return res.rows
  }

  setValueJenisSelected = function ($jenisID) {
    $("input[name='curr_jenis']").val($jenisID);
  }

  addImageCard = function() {
    $idx = Math.floor(Math.random() * 1024);;
    $html = "<div class='col-md-3 mb-2 area-img-"+$idx+"'>";
      $html += '<div class="card" style="position:relative;">';
        $html += '<button type="button" class="btn btn-remove-image" onclick="removeImgCard('+$idx+')">';
          $html += '<span class="fa fa-trash-alt"></span>';
        $html += '</button>';
        $html += '<img src="'+$baseUrl+'/_vendors/themes/images/img-placeholder.jpeg" class="card-img-top" id="img-'+$idx+'">';
        $html += '<div class="card-body">';
          $html += '<input type="file" name="imgMobil[]" accept="image/png, image/jpeg, image/png, image/gif" class="form-control form-control-sm input-img" data-idx='+$idx+' onchange="loadFileImg($(this));">';
        $html += '</div>';
      $html += '</div>';
    $html += "</div>";

    $("#areaImg").append($html);
  }

  removeImgCard = function ($idx) {
    $(".area-img-"+$idx).remove();
  }

  loadFileImg = function($this) {
    var $id = "img-"+$this.data('idx');
    // console.log(event.target.files[0]);
    var image = document.getElementById($id);
    image.src = URL.createObjectURL(event.target.files[0]);
  }

  btnActionMobil = function(value) {
    return [
      '<a href="'+$uri+'/admin/mobil/update/'+value+'" class="btn btn-sm btn-primary" title="edit data">',
      '<i class="fa fa-pencil-alt"></i>',
      '</a> &nbsp;',
      '<a href="'+$uri+'/admin/mobil/delete/'+value+'" onclick="return confirm(\'Are you sure want to delete?\')" class="btn btn-sm btn-danger" title="hapus data">',
      '<i class="fa fa-trash-alt"></i>',
      '</a>',
    ].join('');
  }
  
	// merk
	btnMerk = function() {
		return {
      btnAdd: {
        text: 'Tambah Data',
        icon: 'fa-plus',
        event: function () {
          location.href = $uri+'admin/mobil/merk/add';
        }
      }
    }
	}

  // jenis
  btnJenis = function() {
    return {
      btnAdd: {
        text: 'Tambah Data',
        icon: 'fa-plus',
        event: function () {
          location.href = $uri+'admin/mobil/jenis/add';
        }
      }
    }
  }

  formatNumber = function(value) {
    return accounting.formatNumber(value);
  }



	return {
    init: function() {
      $("body").on('change', '#imerekmobil', function () {
        getJenisByMerkID($(this).val());
      });

      $(function() {
        getJenisByMerkID($("#imerekmobil").val(), $("#curr_jenis").val());
      });

      $("body").on('change', '#ijenismobil', function () {
        setValueJenisSelected($(this).val());
      });

      $("body").on('click','#btnAddImage', function() {
        addImageCard();
      });

      $("body").on('click', '#btnCariMobil', function() {
        $("#tblMobil").bootstrapTable('refresh');
      });
    },
    loadFileImg: function($this) {
      var $id = "img-"+$this.data('idx');
      // console.log(event.target.files[0]);
      var image = document.getElementById($id);
      image.src = URL.createObjectURL(event.target.files[0]);
    }
  }
})();


_mobilJS.init();


