var _usersJS = (function() {
	// mobil
  btnAdd = function() {
    return {
      btnAdd: {
        text: 'Tambah Data',
        icon: 'fa-plus',
        event: function () {
          location.href = $uri+'admin/users/add';
        }
      }
    }
  }


  qParams = function(params) {
    params.tipe       = $("select[name='tipe']").val();
    params.q          = $("input[name='q']").val();
    return params;
  }

  responseHandler = function(res) {
    return res.rows
  }


  btnActionUsers = function(value) {
    return [
      '<a href="'+$uri+'admin/users/update/'+value+'" class="btn btn-sm btn-primary" title="edit data">',
      '<i class="fa fa-pencil-alt"></i>',
      '</a> &nbsp;',
      '<a href="'+$uri+'admin/users/delete/'+value+'" onclick="return confirm(\'Are you sure want to delete?\')" class="btn btn-sm btn-danger" title="hapus data">',
      '<i class="fa fa-trash-alt"></i>',
      '</a>',
    ].join('');
  }
  

  formatNumber = function(value) {
    return accounting.formatNumber(value);
  }

  showPassword = function($el) {
    var tipe = $(".password").attr('type');
    if(tipe == 'password') {
      $(".password").attr('type', 'text');
      $el.html('Sembunyikan Password');
    }else{
      $(".password").attr('type', 'password');
      $el.html('Lihat Password');
    }
  }

	return {
    init: function() {
      $("body").on('click', '#btnCari', function() {
        $("#tblUsers").bootstrapTable('refresh');
      });
    }
  }
})();


_usersJS.init();


