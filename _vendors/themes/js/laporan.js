var _laporanJS = (function() {


  startDatePicker = function () {
    $('#sd').datetimepicker({
      format: 'L',
      locale: 'id'
    });
  }

  endDatePicker = function () {
    $('#ed').datetimepicker({
      format: 'L',
      locale: 'id',
      useCurrent: false
    });
  }


  getFilterBy = function(val) {
    if (val == 'daterange') {
      $("#daterange").removeClass("d-none");
      $("#pengguna").addClass("d-none");
      $("#penggunaInput").val("");
    }else if(val == 'pengguna') {
      $("#pengguna").removeClass("d-none");
      $("#daterange").addClass("d-none");
    }
  }

  getAutocompletePenyewa = function() {
    $("#penggunaInput").autocomplete({
      minLength: 2,
      source: function(request, response) {
        $.ajax({
          url: $uri+'admin/list/penyewa',
          type: 'get',
          dataType: "json",
          data: {
            keyword: request.term
          },
          success: function(res) {
            response(res);
          }
        });
      }
    });
  }

	return {
    init: function() {
      var defaultFilter = $("#selectFilterBy").val();
      getFilterBy(defaultFilter);
      getAutocompletePenyewa();

      $("body").on('change', '#selectFilterBy', function () {
        getFilterBy($(this).val());
      });

      if($("#sd").length) {
        $('#sd').datetimepicker({format: 'DD-MM-YYYY'});
        $("#sd").on("change.datetimepicker", function (e) {
            $('#ed').datetimepicker('minDate', e.date);
        });
      }

      if($("#ed").length) {
        $('#ed').datetimepicker({format: 'DD-MM-YYYY'});
         $("#ed").on("change.datetimepicker", function (e) {
            $('#sd').datetimepicker('maxDate', e.date);
        });
      }

      $("body").on('click', '.btn-print', function() {
        var divToPrint=document.getElementById("printable");
        newWin= window.open("");
        var style = "<style>";
            style += "table {width: 100%;font-size: 14px;}";
            style += "table, th, td {border: solid 1px #DDD; border-collapse: collapse;";
            style += "padding: 3px 4px;text-align: center;}";
            style += "h2 {font-size:18px;}";
            style += "</style>";
        newWin.document.write(style)
        newWin.document.write(divToPrint.outerHTML);
        newWin.print();
        newWin.close();
      });

      

    }
  }
})();


_laporanJS.init();


