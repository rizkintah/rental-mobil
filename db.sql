-- MariaDB dump 10.19  Distrib 10.4.19-MariaDB, for osx10.10 (x86_64)
--
-- Host: localhost    Database: rental_mobil
-- ------------------------------------------------------
-- Server version	10.4.19-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `group_pengguna`
--

DROP TABLE IF EXISTS `group_pengguna`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_pengguna` (
  `group_id` smallint(3) NOT NULL AUTO_INCREMENT,
  `nama_group` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  UNIQUE KEY `nama_group` (`nama_group`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_pengguna`
--

LOCK TABLES `group_pengguna` WRITE;
/*!40000 ALTER TABLE `group_pengguna` DISABLE KEYS */;
INSERT INTO `group_pengguna` VALUES (1,'admin'),(2,'user');
/*!40000 ALTER TABLE `group_pengguna` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jenis_mobil`
--

DROP TABLE IF EXISTS `jenis_mobil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jenis_mobil` (
  `jenis_id` int(11) NOT NULL AUTO_INCREMENT,
  `merk_id` int(11) NOT NULL,
  `nama_jenis` varchar(30) NOT NULL,
  PRIMARY KEY (`jenis_id`),
  KEY `merk_id_jenis_idx` (`merk_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jenis_mobil`
--

LOCK TABLES `jenis_mobil` WRITE;
/*!40000 ALTER TABLE `jenis_mobil` DISABLE KEYS */;
INSERT INTO `jenis_mobil` VALUES (3,2,'Kijang Innova'),(4,2,'New Calya'),(5,2,'New Agya'),(6,3,'Mobilio'),(7,3,'Brio'),(9,7,'Ayla'),(10,7,'Xenia'),(11,7,'Sigra');
/*!40000 ALTER TABLE `jenis_mobil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `merk_mobil`
--

DROP TABLE IF EXISTS `merk_mobil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `merk_mobil` (
  `merk_id` int(11) NOT NULL AUTO_INCREMENT,
  `merk_nama` varchar(45) NOT NULL,
  `merk_icon` text DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `added_at` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`merk_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `merk_mobil`
--

LOCK TABLES `merk_mobil` WRITE;
/*!40000 ALTER TABLE `merk_mobil` DISABLE KEYS */;
INSERT INTO `merk_mobil` VALUES (2,'Toyota','toyota.png',1,'2021-06-19 08:59:56'),(3,'Honda','honda.png',1,'2021-06-19 09:00:35'),(4,'Mitsubishi','mitsubishi.jpg',1,'2021-06-19 09:02:09'),(5,'Datsun','Datsun.png',1,'2021-06-19 09:18:30'),(7,'Daihatshu','daihatsu.png',1,'2021-06-21 04:53:30');
/*!40000 ALTER TABLE `merk_mobil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mobil`
--

DROP TABLE IF EXISTS `mobil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobil` (
  `mobil_id` int(11) NOT NULL AUTO_INCREMENT,
  `warna` varchar(50) NOT NULL,
  `no_polisi` varchar(10) NOT NULL,
  `harga_sewa` decimal(10,0) NOT NULL,
  `jenis_id` int(11) NOT NULL,
  `status` enum('tidak tersedia','tersedia') NOT NULL DEFAULT 'tersedia',
  `inserted_by` int(11) NOT NULL,
  `insert_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`mobil_id`),
  KEY `mobil_fk0` (`jenis_id`),
  KEY `jenis_id` (`jenis_id`),
  CONSTRAINT `mobil_fk0` FOREIGN KEY (`jenis_id`) REFERENCES `jenis_mobil` (`jenis_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mobil`
--

LOCK TABLES `mobil` WRITE;
/*!40000 ALTER TABLE `mobil` DISABLE KEYS */;
INSERT INTO `mobil` VALUES (5,'Red Glossy','B 1479 ABC',350000,4,'tersedia',1,'2021-06-23 14:13:08'),(6,'Bronze Mica Metal','B 1818 XTZ',350000,4,'tersedia',1,'2021-06-23 14:20:10'),(7,'Phoenix Orange ','B 8118 TYA',370000,7,'tersedia',1,'2021-06-23 14:24:54'),(8,'Carnival Yellow','B 9109 JUA',370000,7,'tersedia',1,'2021-06-23 14:27:26');
/*!40000 ALTER TABLE `mobil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mobil_images`
--

DROP TABLE IF EXISTS `mobil_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobil_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mobil_id` int(11) NOT NULL,
  `image_file` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mobil_id_img_idx` (`mobil_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mobil_images`
--

LOCK TABLES `mobil_images` WRITE;
/*!40000 ALTER TABLE `mobil_images` DISABLE KEYS */;
INSERT INTO `mobil_images` VALUES (1,5,'evidence_00004545131.jpeg'),(2,5,'evidence_images1.jpeg'),(3,5,'evidence_toyota-calya-1atc.jpeg'),(4,6,'evidence_Will-Toyota-pitch-its-Calya-MPV-to-battle-it-out-with-Maruti-Suzuki-Ertiga-1.jpeg'),(5,6,'evidence_Will-Toyota-pitch-its-Calya-MPV-to-battle-it-out-with-Maruti-Suzuki-Ertiga-2.jpeg'),(6,6,'evidence_Will-Toyota-pitch-its-Calya-MPV-to-battle-it-out-with-Maruti-Suzuki-Ertiga-3.jpeg'),(7,6,'evidence_Will-Toyota-pitch-its-Calya-MPV-to-battle-it-out-with-Maruti-Suzuki-Ertiga-4.jpeg'),(8,7,'evidence_1637778880p.jpeg'),(9,7,'evidence_download.jpeg'),(10,7,'evidence_honda-brio-2020.jpeg'),(11,8,'evidence_Honda-Brio-12-V-CVT-1024x640.jpeg'),(12,8,'evidence_4764ff8072154a359b26a1b2e2675fbf_750x420.jpeg'),(13,8,'evidence_images2.jpeg');
/*!40000 ALTER TABLE `mobil_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pengguna`
--

DROP TABLE IF EXISTS `pengguna`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pengguna` (
  `pengguna_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` smallint(3) NOT NULL,
  `nama_pengguna` varchar(255) NOT NULL,
  `no_hp` varchar(14) NOT NULL,
  `no_identitas` varchar(30) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` enum('active','not active') NOT NULL DEFAULT 'active',
  `tanggal_registrasi` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `file_ktp` text DEFAULT NULL,
  PRIMARY KEY (`pengguna_id`),
  UNIQUE KEY `no_hp` (`no_hp`),
  UNIQUE KEY `no_identitas` (`no_identitas`),
  UNIQUE KEY `username` (`username`),
  KEY `pengguna_fk0` (`group_id`),
  CONSTRAINT `pengguna_fk0` FOREIGN KEY (`group_id`) REFERENCES `group_pengguna` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pengguna`
--

LOCK TABLES `pengguna` WRITE;
/*!40000 ALTER TABLE `pengguna` DISABLE KEYS */;
INSERT INTO `pengguna` VALUES (1,1,'administrator','','','','','admin','$2y$10$1pwzhRmI9W77z5rn0Gz.suZVObEme8aJ/II3czpfK4Q77vBZ8KZvG','active','2021-06-18 13:49:55',NULL),(2,2,'Edi Rizkinta Harahap','082210541720','327509897r3','Jatibening','rizkinta@gmail.com','rizkinta','$2y$10$1pwzhRmI9W77z5rn0Gz.suZVObEme8aJ/II3czpfK4Q77vBZ8KZvG','active','2021-07-02 03:31:57','idcardsample.jpeg'),(4,2,'Tata Harahap','321312','12313','Bekasi','a@a.com','rizkintah','$2y$10$sJRsGVSCOC7bKKQ9FhjSFuDlLK13uHhy9zMfsyS1UCLVII3Pu/5Fu','active','2021-07-02 17:54:16','photo_2021-06-21_14.57.36.jpeg');
/*!40000 ALTER TABLE `pengguna` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `penyewaan`
--

DROP TABLE IF EXISTS `penyewaan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `penyewaan` (
  `penyewaan_id` varchar(200) NOT NULL,
  `pengguna_id` int(11) NOT NULL,
  `mobil_id` int(11) NOT NULL,
  `lama_sewa` int(5) NOT NULL,
  `total_harga_sewa` decimal(10,0) NOT NULL,
  `tanggal_sewa` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `tanggal_transaksi` datetime NOT NULL DEFAULT current_timestamp(),
  `status_sewa` enum('1','2','3','4','5','6','7') NOT NULL DEFAULT '4' COMMENT '1 => disewa\n2 => selesai sewa\n3 => batal\n4 => Menunggu Pembayaran\n5 => Pembayaran Diterima\n6 => Kendaraan siap diambil',
  `tanggal_dikembalikan` datetime DEFAULT NULL,
  `total_denda` decimal(10,0) DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `bukti_pembayaran` text DEFAULT NULL,
  `waktu_mulai_sewa` time DEFAULT NULL,
  `waktu_kembali` time DEFAULT NULL,
  PRIMARY KEY (`penyewaan_id`),
  KEY `penyewaan_fk0` (`pengguna_id`),
  KEY `penyewaan_fk1` (`mobil_id`),
  KEY `penyewaan_fk2` (`admin_id`),
  CONSTRAINT `penyewaan_fk0` FOREIGN KEY (`pengguna_id`) REFERENCES `pengguna` (`pengguna_id`),
  CONSTRAINT `penyewaan_fk1` FOREIGN KEY (`mobil_id`) REFERENCES `mobil` (`mobil_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `penyewaan`
--

LOCK TABLES `penyewaan` WRITE;
/*!40000 ALTER TABLE `penyewaan` DISABLE KEYS */;
INSERT INTO `penyewaan` VALUES ('2106290001',2,5,2,700000,'2021-06-29','2021-07-01','2021-06-29 17:21:30','2','2021-07-02 10:36:00',NULL,1,'photo6213132895242005450.jpg','14:30:00','14:30:00');
/*!40000 ALTER TABLE `penyewaan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v_jenis_mobil`
--

DROP TABLE IF EXISTS `v_jenis_mobil`;
/*!50001 DROP VIEW IF EXISTS `v_jenis_mobil`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_jenis_mobil` (
  `jenis_id` tinyint NOT NULL,
  `merk_id` tinyint NOT NULL,
  `nama_jenis` tinyint NOT NULL,
  `merk_nama` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_mobil`
--

DROP TABLE IF EXISTS `v_mobil`;
/*!50001 DROP VIEW IF EXISTS `v_mobil`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_mobil` (
  `mobil_id` tinyint NOT NULL,
  `warna` tinyint NOT NULL,
  `no_polisi` tinyint NOT NULL,
  `harga_sewa` tinyint NOT NULL,
  `jenis_id` tinyint NOT NULL,
  `status` tinyint NOT NULL,
  `inserted_by` tinyint NOT NULL,
  `insert_at` tinyint NOT NULL,
  `nama_jenis` tinyint NOT NULL,
  `merk_id` tinyint NOT NULL,
  `merk_nama` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_mobil_thumbnail`
--

DROP TABLE IF EXISTS `v_mobil_thumbnail`;
/*!50001 DROP VIEW IF EXISTS `v_mobil_thumbnail`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_mobil_thumbnail` (
  `mobil_id` tinyint NOT NULL,
  `image_file` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_pengguna`
--

DROP TABLE IF EXISTS `v_pengguna`;
/*!50001 DROP VIEW IF EXISTS `v_pengguna`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_pengguna` (
  `pengguna_id` tinyint NOT NULL,
  `group_id` tinyint NOT NULL,
  `nama_pengguna` tinyint NOT NULL,
  `no_hp` tinyint NOT NULL,
  `no_identitas` tinyint NOT NULL,
  `alamat` tinyint NOT NULL,
  `email` tinyint NOT NULL,
  `username` tinyint NOT NULL,
  `password` tinyint NOT NULL,
  `status` tinyint NOT NULL,
  `tanggal_registrasi` tinyint NOT NULL,
  `nama_group` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `v_jenis_mobil`
--

/*!50001 DROP TABLE IF EXISTS `v_jenis_mobil`*/;
/*!50001 DROP VIEW IF EXISTS `v_jenis_mobil`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_jenis_mobil` AS select `a`.`jenis_id` AS `jenis_id`,`a`.`merk_id` AS `merk_id`,`a`.`nama_jenis` AS `nama_jenis`,`b`.`merk_nama` AS `merk_nama` from (`jenis_mobil` `a` join `merk_mobil` `b`) where `a`.`merk_id` = `b`.`merk_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_mobil`
--

/*!50001 DROP TABLE IF EXISTS `v_mobil`*/;
/*!50001 DROP VIEW IF EXISTS `v_mobil`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_mobil` AS select `A`.`mobil_id` AS `mobil_id`,`A`.`warna` AS `warna`,`A`.`no_polisi` AS `no_polisi`,`A`.`harga_sewa` AS `harga_sewa`,`A`.`jenis_id` AS `jenis_id`,`A`.`status` AS `status`,`A`.`inserted_by` AS `inserted_by`,`A`.`insert_at` AS `insert_at`,`B`.`nama_jenis` AS `nama_jenis`,`C`.`merk_id` AS `merk_id`,`C`.`merk_nama` AS `merk_nama` from ((`mobil` `A` join `jenis_mobil` `B`) join `merk_mobil` `C`) where `A`.`jenis_id` = `B`.`jenis_id` and `B`.`merk_id` = `C`.`merk_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_mobil_thumbnail`
--

/*!50001 DROP TABLE IF EXISTS `v_mobil_thumbnail`*/;
/*!50001 DROP VIEW IF EXISTS `v_mobil_thumbnail`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_mobil_thumbnail` AS select `B`.`mobil_id` AS `mobil_id`,`B`.`image_file` AS `image_file` from ((select min(`rental_mobil`.`mobil_images`.`id`) AS `id`,`rental_mobil`.`mobil_images`.`mobil_id` AS `mobil_id` from `rental_mobil`.`mobil_images` group by `rental_mobil`.`mobil_images`.`mobil_id`) `A` join `rental_mobil`.`mobil_images` `B`) where `a`.`id` = `B`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_pengguna`
--

/*!50001 DROP TABLE IF EXISTS `v_pengguna`*/;
/*!50001 DROP VIEW IF EXISTS `v_pengguna`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_pengguna` AS select `A`.`pengguna_id` AS `pengguna_id`,`A`.`group_id` AS `group_id`,`A`.`nama_pengguna` AS `nama_pengguna`,`A`.`no_hp` AS `no_hp`,`A`.`no_identitas` AS `no_identitas`,`A`.`alamat` AS `alamat`,`A`.`email` AS `email`,`A`.`username` AS `username`,`A`.`password` AS `password`,`A`.`status` AS `status`,`A`.`tanggal_registrasi` AS `tanggal_registrasi`,`B`.`nama_group` AS `nama_group` from (`pengguna` `A` join `group_pengguna` `B`) where `A`.`group_id` = `B`.`group_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-21 20:23:17
