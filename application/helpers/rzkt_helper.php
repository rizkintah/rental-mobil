<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function encryptUrl($param) {
  $CI = get_instance();
  $CI->load->library('encryption');
  return urlencode(base64_encode($CI->encryption->encrypt($param)));
}

function decryptUrl($param) {
  $CI = get_instance();
  $CI->load->library('encryption');
  return $CI->encryption->decrypt(base64_decode(urldecode($param)));
}

function upload_single_file($path, $fileName, $ext)
{
  $CI = get_instance();

  $config['upload_path']      = $path;
  $config['allowed_types']    = $ext;
  $config['overwrite']        = TRUE;
  $config['remove_spaces']    = TRUE;

  $CI->load->library('upload', $config);

  if ( ! $CI->upload->do_upload($fileName))
  {
    $error = array('error' => $CI->upload->display_errors());
    return $error;
  }
  else
  {
    $uploadData = $CI->upload->data();
    return array('error' => 'no', 'file_name' => $uploadData['file_name']);
  }
}

function upload_multiple_files($path, $files, $ext) {
  $CI = get_instance();
  
  $images = $arrImg = array();
  foreach ($files['name'] as $key => $image) {
      $_FILES['images[]']['name']= $files['name'][$key];
      $_FILES['images[]']['type']= $files['type'][$key];
      $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
      $_FILES['images[]']['error']= $files['error'][$key];
      $_FILES['images[]']['size']= $files['size'][$key];

      // $fileName = 'evidence_'.$image;

      // $images[] = $fileName;

      $config['upload_path'] = $path; 
      $config['allowed_types'] = $ext;
      $config['overwrite']        = TRUE;
      $config['remove_spaces']    = TRUE;

      $CI->load->library('upload', $config);
      $config['file_name'] = $image;

      $CI->upload->initialize($config);

      if ($CI->upload->do_upload('images[]')) {
          $uploadData = $CI->upload->data();
          $imgFileName = $uploadData['file_name'];
          $arrImg[] = $imgFileName;
      } else {
          return false;
      }
  }
  return $arrImg;
}

function reverseDate($date, $divider = "/")
{
  $newTanggal = implode('-', array_reverse(explode($divider, $date)));
  return $newTanggal;
}

/*
  1 => disewa
  2 => selesai sewa
  3 => batal
  4 => Menunggu Pembayaran
  5 => Pembayaran Diterima
  6 => Kendaraan siap diambil
*/
function getStatusName($statusID) {
  $arr = array(
    1 => 'Disewa',
    2 => 'Selesai Sewa',
    3 => 'Batal',
    4 => 'Menunggu Pembayaran',
    5 => 'Pembayaran Diterima',
    6 => 'Kendaraan Siap Diambil',
    7 => 'Menunggu Verifikasi Pembayaran',
    'Disewa'  => 1,
    'Selesai Sewa'  => 2,
    'Batal' => 3,
    'Menunggu Pembayaran' => 4,
    'Pembayaran Diterima' => 5,
    'Kendaraan Siap Diambil'  => 6,
    'Menunggu Verifikasi Pembayaran' => 7
  );

  return $arr[$statusID];
}

/* End of file rzkt_helper.php */
/* Location: ./application/helpers/rzkt_helper.php */