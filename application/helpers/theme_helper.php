<?php  
	function theme_frontend($page,$data)
	{
		$CI = get_instance();
		$CI->load->view('themes/header_frontend',$data);
		$CI->load->view($page,$data);
		$CI->load->view('themes/footer_fronted',$data);
	}

  function theme_backend($page,$data)
  {
    $CI = get_instance();
    $CI->load->view('themes/header_backend',$data);
    $CI->load->view($page,$data);
    $CI->load->view('themes/footer_backend',$data);
  }
?>
