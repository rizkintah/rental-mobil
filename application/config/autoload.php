<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] = array();

$autoload['libraries'] = array('database','session','form_validation','encryption','MY_Form_validation','bs_table','rzkt');

$autoload['drivers'] = array();

$autoload['helper'] = array('theme','login','url','inflector','form','security','rzkt', 'date');

$autoload['config'] = array('apps_setting');

$autoload['model'] = array();
