<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'frontend';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

# route frontend
	$route['list_jenis'] 								= 'frontend/get_list_jenis_by_merk';
	$route['lists'] 										= 'frontend/get_list_mobil';
	$route['sewa/(:any)'] 							= 'frontend/get_sewa_mobil/$1';
	$route['login'] 										= 'login/login_user';
	$route['logout'] 										= 'login/logout';
	$route['daftar'] 										= 'login/get_daftar';
# end route frontend

# user / penyewa dashboard
	$route['user/penyewaan'] 						= 'frontend/dashboard/get_list_user_sewa';
	$route['konfirmasi'] 								= 'frontend/dashboard/get_data_konfirmasi';
	$route['invoice/(:any)'] 						= 'frontend/dashboard/get_invoice/$1';
# end user / penyewa dashboard

# route backend
$route['admin'] 											= 'login';
$route['admin/dashboard'] 						= 'backend/dashboard';
$route['admin/penyewaan'] 						= 'backend/penyewaan/get_penyewaan';
$route['admin/penyewaan/list'] 				= 'backend/penyewaan/get_list_penyewaan';
$route['admin/verifikasi/(:any)/(:any)'] 		= 'backend/penyewaan/get_verifikasi/$1/$1';

$route['admin/mobil'] 								= 'backend/mobil';
$route['admin/mobil/lists'] 					= 'backend/mobil/get_list_mobil';
$route['admin/mobil/add'] 						= 'backend/mobil/add_mobil';
$route['admin/mobil/update/(:any)'] 	= 'backend/mobil/update_mobil/$1';
$route['admin/mobil/delete/(:any)'] 	= 'backend/mobil/delete_mobil/$1';
$route['admin/mobil/lists/jenis'] 		= 'backend/mobil/get_list_jenis_by_merk';

$route['admin/mobil/merk'] 								= 'backend/mobil/merk';
$route['admin/mobil/merk/add'] 						= 'backend/mobil/add_merk';
$route['admin/mobil/merk/update/(:any)'] 	= 'backend/mobil/update_merk/$1';
$route['admin/mobil/merk/delete/(:any)'] 	= 'backend/mobil/delete_merk/$1';

$route['admin/mobil/jenis'] 								= 'backend/mobil/jenis';
$route['admin/mobil/jenis/add'] 						= 'backend/mobil/add_jenis';
$route['admin/mobil/jenis/update/(:any)'] 	= 'backend/mobil/update_jenis/$1';
$route['admin/mobil/jenis/delete/(:any)'] 	= 'backend/mobil/delete_jenis/$1';


$route['admin/users'] 											= 'backend/users';
$route['admin/users/lists'] 								= 'backend/users/get_list_users';
$route['admin/users/add'] 									= 'backend/users/add_users';
$route['admin/users/update/(:any)'] 				= 'backend/users/update_users/$1';
$route['admin/users/delete/(:any)'] 				= 'backend/users/delete_users/$1';

$route['admin/laporan/penyewaan'] 					= 'backend/laporan/penyewaan';
$route['admin/list/penyewa'] 							  = 'backend/laporan/get_list_penyewa_by';



# end route backend