<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bs_table {

  public function __construct()
  {
      $this->CI =& get_instance();
  }

  function create_query($query, $limit, $offset, $sort, $order) {
    $orderBy = null;
    
    if ((!empty($sort)) && (!empty($order))) {
      $orderBy = "ORDER BY $sort $order";
    }

    $qData = $this->CI->db->query(" {$query}
                                    LIMIT {$offset}, {$limit}"
                                 );

    return $qData->result_array();
  }

  function create_query_count($query) {
    $qData = $this->CI->db->query("{$query}")->num_rows();
    return $qData;
  }

}

/* End of file Bs_table.php */
/* Location: ./application/libraries/Bs_table.php */