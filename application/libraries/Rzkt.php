<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rzkt {

	private $apiMikrotik;
  private $ci;

	public function __construct()
	{
		$this->ci = get_instance();
	}

	function getTransaksiID() {
		$this->ci->load->database();
		$this->ci->db->select('MAX(penyewaan_id) AS max_kode');
		$data = $this->ci->db->get('penyewaan')->row();
		$codeID = $data->max_kode;
		$urutan = (int) substr($codeID, 3, 3);
		$urutan++;

		$prefix = date('ymd');
		$id = $prefix. sprintf("%04s", $urutan);
		return $id;
	}

	function updateStatusCancelByBatasKonfirmasi() {
		$this->ci->load->database();
		$this->ci->db->query("UPDATE penyewaan
													SET status_sewa = 3
													WHERE status_sewa = 4
													AND TIME_TO_SEC(TIMEDIFF(batas_konfirmasi, NOW())) <= 0");
		return true;
	}
}

/* End of file Rzkt.php */
/* Location: ./application/libraries/Rzkt.php */