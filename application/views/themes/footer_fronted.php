
		</div>
		<!-- jQuery -->
		<script type="text/javascript">
			var $uri = '<?=site_url()?>', 
					$baseUrl = '<?=base_url()?>', 
					$checkUri = "<?= empty($this->uri->segment_array()) ? true : false?>",
					$uriPenyewaan = "<?=$this->uri->segment(2)?>";

		</script>
		<script src="<?=base_url('_vendors/plugins/jquery/jquery.min.js')?>"></script>
		<script src="<?=base_url('_vendors/plugins/bootstrap/js/bootstrap.min.js')?>"></script>
		<script src="<?=base_url('_vendors/plugins/accounting/accounting.min.js')?>"></script>
		<script src="<?=base_url('_vendors/plugins/bootstrap-input-spinner/bootstrap-input-spinner.js')?>"></script>
		<script src="<?=base_url('_vendors/plugins/select2/js/select2.min.js')?>"></script>
		<script src="<?=base_url('_vendors/plugins/jquery-validation/jquery.validate.min.js')?>"></script>
		<script src="<?=base_url('_vendors/plugins/jquery-validation/additional-methods.min.js')?>"></script>
		<script src="<?=base_url('_vendors/plugins/momentjs/moment.min.js')?>"></script>
		<script src="<?=base_url('_vendors/plugins/momentjs/id.min.js')?>"></script>
		<script src="<?=base_url("_vendors/plugins/sweetalert2/sweetalert2.min.js")?>"></script>
		<script src="<?=base_url('_vendors/plugins/tempusdominus/tempusdominus-bootstrap-4.min.js')?>"></script>
		<script src="<?=base_url('_vendors/themes/js/apps.js?v'.date('is'))?>"></script>

		<div class="modal fade" id="modalApps" data-bs-backdrop="static">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h6 class="modal-title"></h6>
		        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
		      </div>
		      <div class="modal-body">
		      </div>
		    </div>
		  </div>
		</div>

		<?php if($this->session->flashdata('notif')): ?>
	    <script type="text/javascript">
	      Swal.fire({
	        position: 'top-end',
	        icon: 'info',
	        title: '<?=$this->session->flashdata('notif')?>',
	        // toast: true,
	        showConfirmButton: false,
	        timer: 3700
	      });
	    </script>
	  <?php endif; ?>
		
		
	</body>
</html>


