<nav id="sidebarMenu" class="col-md-4 col-lg-2 d-lg-block bg-dark  sidebar collapse">
  <div class="position-sticky pt-3">
    
    <!-- dibuka hanya utk tipe direktur -->
    <?php if ($this->session->userdata('levelID') == 1): ?>
      
    <ul class="nav flex-column">
      <li class="nav-item">
        <a class="nav-link " href="<?=site_url('admin/users')?>">
          <span data-feather="users"></span>
          Manajemen Pengguna
        </a>
      </li>
    </ul>

    <ul class="nav flex-column mb-4">
      <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
      	Manajemen Data Kendaraan
      	<span data-feather="edit"></span>
      </h6>
      <li class="nav-item">
        <a class="nav-link " href="<?=site_url('admin/mobil/merk')?>">
          <span data-feather="database"></span>
          Data Merk Mobil
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link " href="<?=site_url('admin/mobil/jenis')?>">
          <span data-feather="database"></span>
          Data Tipe Mobil
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link " href="<?=site_url('admin/mobil')?>">
          <span data-feather="database"></span>
          Data Mobil
        </a>
      </li>
    </ul>

    <?php endif ?>


    <ul class="nav flex-column mb-4">
      <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
        Transaksi Penyewaan
        <span data-feather="list"></span>
      </h6>
      <li class="nav-item">
        <a class="nav-link " href="<?=site_url('admin/penyewaan')?>">
          <span data-feather="briefcase"></span>
          Penyewaan
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link " href="<?=site_url('admin/laporan/penyewaan')?>">
          <span data-feather="book-open"></span>
          Laporan Penyewaan
        </a>
      </li>
      
    </ul>
    
  </div>
</nav>