		 		</main>
		  </div>
		</div>

		<script type="text/javascript">
			var $uri = '<?=site_url()?>', $baseUrl = '<?=base_url()?>';
		</script>
		<script src="<?=base_url('_vendors/plugins/jquery/jquery.min.js')?>"></script>
		<script src="<?=base_url('_vendors/plugins/bootstrap/js/bootstrap.min.js')?>"></script>
		<script src="<?=base_url('_vendors/plugins/accounting/accounting.min.js')?>"></script>
		<script src="<?=base_url('_vendors/plugins/bootstrap-table/bootstrap-table.min.js')?>"></script>
		<script src="<?=base_url('_vendors/plugins/bootstrap-table/extensions/filter-control/bootstrap-table-filter-control.min.js')?>"></script>
		<script src="<?=base_url('_vendors/plugins/bootstrap-table/extensions/fixed-columns/bootstrap-table-fixed-columns.min.js')?>"></script>
		<script src="<?=base_url('_vendors/plugins/bootstrap-table/extensions/mobile/bootstrap-table-mobile.min.js')?>"></script>
		<script src="<?=base_url('_vendors/plugins/momentjs/moment.min.js')?>"></script>
		<script src="<?=base_url('_vendors/plugins/tempusdominus/tempusdominus-bootstrap-4.min.js')?>"></script>
		<script src="<?=base_url('_vendors/plugins/feather-icon/feather.min.js')?>"></script>
		<script src="<?=base_url("_vendors/plugins/sweetalert2/sweetalert2.min.js")?>"></script>
		<script src="<?=base_url("_vendors/plugins/bootstrap-fileinput/js/fileinput.min.js")?>"></script>
		<script src="<?=base_url("_vendors/plugins/bootstrap-fileinput/themes/fas/theme.min.js")?>"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
		<!-- <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css"> -->


		<?php if (file_exists(FCPATH.'_vendors/themes/js/'.$this->uri->segment(2).'.js')): ?>
			<script src="<?=base_url('_vendors/themes/js/'.$this->uri->segment(2).'.js?v='.date('is'))?>"></script>
		<?php endif ?>

    <script type="text/javascript">
    	feather.replace();
    </script>


    <?php if($this->session->flashdata('notif')): ?>
	    <script type="text/javascript">
	      Swal.fire({
	        position: 'top-end',
	        icon: 'info',
	        title: '<?=$this->session->flashdata('notif')?>',
	        // toast: true,
	        showConfirmButton: false,
	        timer: 3700
	      });
	    </script>
	  <?php endif; ?>
	</body>
</html>

<div class="modal fade" id="modalApps" data-bs-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title"></h6>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>
