<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?=$this->config->item('apps_title')?>  <?=$title ? ' - '.$title : null?></title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="<?=base_url('_vendors/plugins/bootstrap/css/bootstrap.min.css')?>">
		<link rel="stylesheet" href="<?=base_url('_vendors/plugins/animatecss/animate.min.css')?>">
  	<link rel="stylesheet" href="<?=base_url("_vendors/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css")?>">
		<link rel="stylesheet" href="<?=base_url('_vendors/plugins/placeholder-loading/placeholder-loading.min.css')?>">
		<link rel="stylesheet" href="<?=base_url("_vendors/plugins/fontawesome-free/css/all.min.css")?>">
		<link rel="stylesheet" href="<?=base_url('_vendors/plugins/select2/css/select2.min.css')?>">
		<link rel="stylesheet" href="<?=base_url('_vendors/plugins/tempusdominus/tempusdominus-bootstrap-4.min.css')?>">
		<link rel="stylesheet" href="<?=base_url('_vendors/plugins/select2-bootstrap5/select2-bootstrap5.min.css')?>">
		<link rel="stylesheet" href="<?=base_url('_vendors/themes/css/main.css?v='.date('is'))?>">

		<link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@200;300;400;500&display=swap" rel="stylesheet">

		<link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;300;400&display=swap" rel="stylesheet">



		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark shadow-lg">
		  <div class="container">
		    <a class="navbar-brand" href="<?=site_url()?>">
		    	<?=$this->config->item('apps_title')?>
		    	<br>
		    </a>
		    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		      <span class="navbar-toggler-icon"></span>
		    </button>
		    
		    <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    	<div class="">
	      		<a href="#" class="d-flex justify-content-start align-items-center text-decoration-none text-white">
	      			<svg xmlns="http://www.w3.org/2000/svg" class="icon-md me-1" viewBox="0 0 20 20" fill="currentColor">
							  <path d="M2 3a1 1 0 011-1h2.153a1 1 0 01.986.836l.74 4.435a1 1 0 01-.54 1.06l-1.548.773a11.037 11.037 0 006.105 6.105l.774-1.548a1 1 0 011.059-.54l4.435.74a1 1 0 01.836.986V17a1 1 0 01-1 1h-2C7.82 18 2 12.18 2 5V3z" />
							</svg>
							0838-0885-5891
	      		</a>
		      </div>
		      <div class="mb-3 d-md-block d-lg-none"></div>
		      <?php if (!is_logged()): ?>

		      	<div class="ms-auto">
		      		<a href="<?=site_url('login')?>" class="btn btn-outline-light me-2">Masuk</a>
		      		<a href="<?=site_url('daftar')?>" class="btn btn-warning">Daftar</a>
			      </div>

		      <?php else: ?>
		      	<ul class="navbar-nav ms-auto mb-2 mb-lg-0">
			        <li class="nav-item">
			          <a class="nav-link active" aria-current="page" href="<?=site_url('user/penyewaan')?>">
			          	Daftar Penyewaan
			          </a>
			        </li>
			        <li class="nav-item">
			          <a class="nav-link active" aria-current="page" href="#">|</a>
			        </li>
			        <li class="nav-item dropdown">
			          <a class="nav-link dropdown-toggle dropend" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
			            <i class="fas fa-user me-1"></i> <?=$this->session->userdata('nama')?>
			          </a>
			          <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
			            <li>
			            	<a class="dropdown-item" href="<?=site_url('logout')?>">
			            		<i class="fas fa-sign-out-alt"></i> Keluar
			            	</a>
			            </li>
			          </ul>
			        </li>
			      </ul>
		      <?php endif ?>
		      
		    </div>
		  </div>
		</nav>

		<?php if (!$this->uri->segment(1)): ?>			
			<div class="jumbotron-area text-center">
				<h1 class="display-4 animate__animated animate__fadeInLeft">Sewa mobil jadi lebih mudah</h1>
		  </div>
		<?php endif ?>


	 	<div class="container">

