<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?=$this->config->item('apps_title')?> - Admin Dashboard</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="<?=base_url('_vendors/plugins/bootstrap/css/bootstrap.min.css')?>">
		<link rel="stylesheet" href="<?=base_url('_vendors/plugins/bootstrap-table/bootstrap-table.min.css')?>">
		<link rel="stylesheet" href="<?=base_url('_vendors/plugins/bootstrap-table/extensions/fixed-columns/bootstrap-table-fixed-columns.min.css')?>">
		<link rel="stylesheet" href="<?=base_url('_vendors/plugins/tempusdominus/tempusdominus-bootstrap-4.min.css')?>">
  	<link rel="stylesheet" href="<?=base_url("_vendors/plugins/fontawesome-free/css/all.min.css")?>">
  	<link rel="stylesheet" href="<?=base_url("_vendors/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css")?>">
  	<link rel="stylesheet" href="<?=base_url("_vendors/plugins/bootstrap-fileinput/css/fileinput.min.css")?>">
		<link rel="stylesheet" href="<?=base_url('_vendors/themes/css/backend.css?v='.date('is'))?>">

		<link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@200;300;400;500&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200;400;500&display=swap" rel="stylesheet">


		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>

		<header class="navbar navbar-expand-lg sticky-top navbar-light bg-warning flex-md-nowrap p-0 shadow">
		  <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3 text-center" href="#">
		  	<?=$this->config->item('apps_title')?>
		  </a>
		  <button class="navbar-toggler position-absolute d-lg-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		  <ul class="navbar-nav px-3 ms-auto d-md-none d-lg-block">
		    <!-- <li class="nav-item text-nowrap">
		      <a class="nav-link" href="#">
		      	<span data-feather="log-out" class="feather-16"></span> Keluar
		    	</a>
		    </li> -->
		    <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            <span data-feather="user" class="feather-16"></span> Hai, <?=$this->session->userdata('nama')?>
          </a>
          <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDropdown">
            <li>
            	<a class="dropdown-item" href="<?=site_url('logout')?>">
            		<span data-feather="log-out" class="feather-16"></span> Keluar
            	</a>
            </li>
          </ul>
        </li>
		  </ul>
		</header>

		<div class="container-fluid">
		  <div class="row">
		    <?php include_once 'sidebar_backend.php'; ?>

		    <main class="col-md-12 ms-sm-auto col-lg-10 px-md-4">
		      <!-- <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
		        <h1 class="h4">Dashboard</h1>
		      </div> -->


		

