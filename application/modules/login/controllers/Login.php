<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Login_model','mod_login');
		$this->config->set_item('language', 'id'); 
	}

	public function index()
	{
		$this->form_validation->set_rules('un', 'Username', 'trim|required');
		$this->form_validation->set_rules('pw', 'Password', 'required');
		
		if ($this->form_validation->run() === TRUE) {
			$un = $this->input->post('un');
			$pw = $this->input->post('pw');

			$login = $this->_proses_login($un, $pw);
			if ($login['status'] == 'sukses') {
				redirect('admin/penyewaan');
			}else{
				$this->session->set_flashdata('notif', $login['err']);
        redirect('admin');
			}
		}else{
			$this->load->view('index');
		}
	}

	function _proses_login($un, $pw)
	{
		$authCheck = $this->mod_login->auth($un);
		$err = array();
		
		if ($authCheck == false) {
			$err = array('status' => 'gagal', 'err' => 'Username yang anda masukan salah');
		}elseif (password_verify($pw, $authCheck->password)) {
			$dataSession = array(
        'userID'    => $authCheck->pengguna_id,
        'username'  => $authCheck->username,
        'nama'			=> $authCheck->nama_pengguna,
        'level'			=> $authCheck->nama_group,
        'levelID'	 	=> $authCheck->group_id,
        'logging'   => true
      );
      $this->session->set_userdata($dataSession);
      $err = array('status' => 'sukses');
		}else{
			$err = array('status' => 'gagal', 'err' => 'Username dan Password yang anda masukan salah');
		}
		return $err;
	}

	function login_user() {

		$this->form_validation->set_rules('un', 'Username', 'trim|required');
		$this->form_validation->set_rules('pw', 'Password', 'required');

		

		$mid 		= decryptUrl($this->input->post('mid'));
		$hari 	= $this->input->post('hari');
		$tsewa 	= $this->input->post('tsewa');
		$url 		= site_url();

		
		if (!empty($mid)|| !empty($hari) || !empty($tsewa)) 
			$url = site_url('sewa/'.encryptUrl($mid).'?hari='.$hari.'&tsewa='.$tsewa);

		if ($this->form_validation->run() == TRUE) {
			$un = $this->input->post('un');
			$pw = $this->input->post('pw');

			$login = $this->_proses_login_user($un, $pw);
		
			if ($login['status'] == 'sukses') {
				// redirect('user/dashboard');
				redirect($url);
			}else{
				$this->session->set_flashdata('notif', $login['err']);
				if (!empty($mid)) {
	        redirect('login?mid='.encryptUrl($mid).'&hari='.$hari.'&tsewa='.$tsewa);
				}else{
					redirect('login');
				}
			}
		} else {
			$data['title'] = "Login";
			theme_frontend('login/login_front',$data);
		}
	}

	function _proses_login_user($un, $pw)
	{
		$authCheck = $this->mod_login->auth_user($un);
		$err = array();
		
		if ($authCheck == false) {
			$err = array('status' => 'gagal', 'err' => 'Username yang anda masukan salah');
		}elseif (password_verify($pw, $authCheck->password)) {
			$dataSession = array(
        'userID'    => $authCheck->pengguna_id,
        'username'  => $authCheck->username,
        'nama'			=> $authCheck->nama_pengguna,
        'level'			=> $authCheck->nama_group,
        'levelID'	 	=> $authCheck->group_id,
        'logging'   => true
      );
      $this->session->set_userdata($dataSession);
      $err = array('status' => 'sukses');
		}else{
			$err = array('status' => 'gagal', 'err' => 'Username dan Password yang anda masukan salah');
		}
		return $err;
	}

	# daftar
		function get_daftar()
		{
			$this->form_validation->set_rules('naleng', 'Nama Lengkap', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[pengguna.email]');
			$this->form_validation->set_rules('identitas', 'No. Identitas', 'trim|required|is_numeric|is_unique[pengguna.no_identitas]');
			$this->form_validation->set_rules('nohp', 'No. Handphone', 'trim|required|is_numeric|is_unique[pengguna.no_hp]');
			$this->form_validation->set_rules('un', 'Username', 'trim|required|is_unique[pengguna.username]');
			$this->form_validation->set_rules('pw', 'Password', 'required');
			$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');

			if (empty($_FILES['filektp']['name']))
			  $this->form_validation->set_rules('filektp', 'File Identitas (KTP)', 'required');


			if ($this->form_validation->run() == TRUE) {
				$uploadKtp = upload_single_file('./_files/_ktp', 'filektp', '*');
				$_POST['pw'] = password_hash($_POST['pw'], PASSWORD_DEFAULT);
				$_POST['file_ktp'] = $uploadKtp['file_name'];
				$insert = $this->mod_login->insertDaftar($_POST);
				if ($insert) {
					$this->session->set_flashdata('notif', 'Pendaftaran berhasil, silahkan masuk dengan menggunakan username dan password yang telah anda daftarkan.');
					redirect('login');
				}else{
					$this->session->set_flashdata('notif', 'Pendaftaran gagal, silahkan masukan data dengan benar dan sesuai.');
					redirect('daftar');
				}
			}else{
				$data['title'] = "Daftar";
				theme_frontend('login/daftar_front',$data);
			}
		}
	# end daftar

	function create_pw()
	{
		// echo password_hash("admin123", PASSWORD_DEFAULT);
		echo password_hash("123456", PASSWORD_DEFAULT);
	}

	function logout(){
		$level =$this->session->userdata('level');
    $this->session->sess_destroy();
		if ($level == 'admin') {
			redirect(site_url('admin'));
		}else{
    	redirect(site_url());
		}
  }
}

/* End of file Login.php */
/* Location: ./application/modules/login/controllers/Login.php */