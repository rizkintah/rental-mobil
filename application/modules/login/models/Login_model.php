<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

	private $vUser = 'v_pengguna';
	private $tPengguna = 'pengguna';

	function auth($un) {
		$this->db->where('username', $un);
		$this->db->where_in('group_id', array(1,3));
		$check = $this->db->get($this->vUser);
		if ($check->num_rows() == 1) {
			return $check->row();
		}
		return false;
	}

	function auth_user($un) {
		$this->db->where('username', $un);
		$this->db->where('group_id', 2);
		$check = $this->db->get($this->vUser);
		if ($check->num_rows() == 1) {
			return $check->row();
		}
		return false;
	}

	function insertDaftar($data) {
		$arrData = array(
			'group_id'			=> 2,
			'nama_pengguna'	=> $data['naleng'],
			'no_hp'					=> $data['nohp'],
			'no_identitas'	=> $data['identitas'],
			'alamat'				=> $data['alamat'],
			'email'					=> $data['email'],
			'username'			=> $data['un'],
			'password'			=> $data['pw'],
			'file_ktp'			=> $data['file_ktp'],
		);

		return $this->db->insert($this->tPengguna, $arrData);
	}

}

/* End of file Login_model.php */
/* Location: ./application/modules/login/models/Login_model.php */