<?php  
	$url = site_url('daftar');
	$mid = $this->input->get('mid');
	$hari = $this->input->get('hari');
	$tsewa = $this->input->get('tsewa');
	if (!empty($mid) && !empty($hari) && !empty($tsewa)) 
		$url = site_url('daftar?mid='.$mid.'&hari='.$hari.'&tsewa='.$tsewa); 
?>

<div class="row">
	<div class="col-md-4 mx-auto mt-5">
		<div class="card text-dark bg-white mb-3 shadow-sm border-0">
		  <div class="card-header bg-white py-3 text-center">
		  	<strong>Silahkan masuk ke akun kamu</strong>
		  </div>
		  <div class="card-body px-4">
		   	<form action="<?=current_url()?>" method="POST" class="mt-3">
		   		<input type="hidden" name="mid" value="<?=set_value('mid', $mid)?>">
		   		<input type="hidden" name="hari" value="<?=set_value('hari', $hari)?>">
		   		<input type="hidden" name="tsewa" value="<?=set_value('tsewa', $tsewa)?>">
		   		<div class="mb-3">
					  <label for="iun" class="form-label mb-1">Username</label>
					  <input type="text" name="un" class="form-control <?=form_error('un') ? 'is-invalid' : null?>" 
					  			 id="iun" placeholder="Masukan username">
					  <?php echo form_error('un','<div class="invalid-feedback"><small>', '</small></div>'); ?>
					</div>
					<div class="mb-4">
					  <label for="ipw" class="form-label mb-1">Password</label>
					  <input type="password" name="pw" class="form-control <?=form_error('pw') ? 'is-invalid' : null?>" 
					  			 id="ipw" placeholder="Masukan password">
					  <?php echo form_error('pw','<div class="invalid-feedback"><small>', '</small></div>'); ?>
					</div>

					<div class="d-grid gap-1">
						<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>">
						<button type="submit" class="btn btn-warning">Masuk</button>
					</div>
		   	</form>

		   	<h6 class="border-between">
		   		<span class="bg-white">atau</span>
		   	</h6>

		   	<div class="d-grid mb-2">
					<a href="<?=$url?>" class="btn btn-dark">Daftar</a>
				</div>

		  </div>
		</div>
	</div>
</div>
