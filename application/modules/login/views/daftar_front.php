<div class="row">
	<div class="col-md-6 mx-auto mt-5">
		<div class="card text-dark bg-white mb-3 shadow-sm border-0">
		  <div class="card-header bg-white py-3 text-center">
		  	<strong>Silahkan masukan data diri kamu</strong>
		  </div>
		  <div class="card-body px-4">
		   	<form action="<?=current_url()?>" method="POST" id="frmReg" class="mt-2 row g-3" enctype="multipart/form-data">
		   		
		   		<div class="col-md-12">
					  <label for="inaleng" class="form-label mb-1">Nama Lengkap</label>
					  <input type="text" name="naleng" class="form-control <?=form_error('naleng') ? 'is-invalid' : null?>" 
					  			 id="inaleng" 
					  			 placeholder="Masukan nama lengkap"

					  			 value="<?=set_value('naleng')?>">
					  <?php echo form_error('naleng','<div class="invalid-feedback"><small>', '</small></div>'); ?>
					</div>

					<div class="col-md-12">
					  <label for="iemail" class="form-label mb-1">Alamat Email</label>
					  <input type="email" name="email" class="form-control <?=form_error('email') ? 'is-invalid' : null?>" 
					  			 id="iemail" placeholder="Masukan alamat email"
					  			 value="<?=set_value('email')?>">
					  <?php echo form_error('email','<div class="invalid-feedback"><small>', '</small></div>'); ?>
					</div>

					<div class="col-md-6">
					  <label for="iidentitas" class="form-label mb-1">No. Identitas</label>
					  <input type="text" name="identitas" class="form-control <?=form_error('identitas') ? 'is-invalid' : null?>"
					  			 id="iidentitas" placeholder="Masukan no. identitas (ktp)"
					  			 value="<?=set_value('identitas')?>">
					  <?php echo form_error('identitas','<div class="invalid-feedback"><small>', '</small></div>'); ?>
					</div>

					<div class="col-md-6">
					  <label for="inohp" class="form-label mb-1">Nomor Handphone</label>
					  <input type="text" name="nohp" class="form-control <?=form_error('nohp') ? 'is-invalid' : null?>" 
					  			 id="inohp" placeholder="Masukan no. handphone"
					  			 value="<?=set_value('nohp')?>">
					  <?php echo form_error('nohp','<div class="invalid-feedback"><small>', '</small></div>'); ?>
					</div>

					<div class="col-md-6">
					  <label for="iun" class="form-label mb-1">Username</label>
					  <input type="text" name="un" class="form-control <?=form_error('un') ? 'is-invalid' : null?>" 
					  			 id="iun" placeholder="Masukan username"
					  			 value="<?=set_value('un')?>">
					  <?php echo form_error('un','<div class="invalid-feedback"><small>', '</small></div>'); ?>
					</div>

					<div class="col-md-6">
					  <label for="ipw" class="form-label mb-1">Password</label>
					  <div class="input-group">
						  <input type="password" name="pw" class="form-control password <?=form_error('pw') ? 'is-invalid' : null?>" 
					  			 id="ipw" placeholder="Masukan password"
					  			 value="<?=set_value('pw')?>">
					  	<button class="btn btn-outline-secondary" type="button" title="Show / Hide Password" onclick="showPassword($(this))">
					  		<i class="fa fa-eye"></i>
					  	</button>
					  	<?php echo form_error('pw','<div class="invalid-feedback"><small>', '</small></div>'); ?>
						</div>
					</div>

					<div class="col-md-12">
					  <label for="ifile" class="form-label mb-1">Upload Foto KTP</label>
					  <input type="file" name="filektp" class="form-control <?=form_error('filektp') ? 'is-invalid' : null?>" 
					  			 id="ifile" accept="image/*">
					  <?php echo form_error('filektp','<div class="invalid-feedback"><small>', '</small></div>'); ?>
					</div>

					<div class="col-md-12">
					  <label for="ialamat" class="form-label mb-1">Alamat</label>
					  <textarea name="alamat" id="ialamat" class="form-control <?=form_error('alamat') ? 'is-invalid' : null?>" rows="2"  style="resize:none;" placeholder="Masukan alamat"><?=set_value('alamat')?></textarea>
					  <?php echo form_error('alamat','<div class="invalid-feedback"><small>', '</small></div>'); ?>
					</div>
					<div class="d-grid gap-1">
						<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>">
						<button type="submit" class="btn btnReg btn-dark">Daftar</button>
					</div>
		   	</form>
		  </div>
		</div>
	</div>
</div>
