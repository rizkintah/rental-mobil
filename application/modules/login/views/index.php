<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Login Admin</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="<?=base_url('_vendors/plugins/bootstrap/css/bootstrap.min.css')?>">
		<link rel="stylesheet" href="<?=base_url('_vendors/themes/css/backend.css?v='.date('is'))?>">

		<link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@200;300;400;500&display=swap" rel="stylesheet">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="login-area">

		<main class="form-signin bg-body rounded shadow">
		  <form action="<?=current_url()?>" method="POST">
		    <h1 class="h3 mb-3 fw-500 text-center">
		    	LOGIN
		    </h1>
		    
		    <center class="mb-3 text-secondary">
			    <small style="font-size: .775em;">
			    	Silahkan masuk dengan username dan password.
			    </small>
		    </center>

		    <?php if ($this->session->flashdata('notif')): ?>
		    	<div class="alert alert-danger" role="alert">
					  <small><?=$this->session->flashdata('notif')?></small>
					</div>
		    <?php endif ?>

		    <div class="form-floating">
		      <input type="text" name="un" 
		      			 class="form-control <?=form_error('un') ? 'is-invalid' : null?>" 
		      			 id="iun" 
		      			 value="<?=set_value('un')?>"
		      			 placeholder="Username">
		      <label for="iun">Username</label>
		      <?php echo form_error('un','<div class="invalid-feedback">', '</div>'); ?>
		    </div>
		    <div class="form-floating mt-2">
		      <input type="password"  name="pw" 
		      			class="form-control <?=form_error('pw') ? 'is-invalid' : null?>" 
		      			id="ipw" 
		      			value="<?=set_value('password')?>"
		      			placeholder="Password">
		      <label for="ipw">Password</label>
		      <?php echo form_error('pw','<div class="invalid-feedback">', '</div>'); ?>
		    </div>
		    <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>">
		    <button class="w-100 fw-light btn btn-lg btn-warning text-dark mt-3" type="submit">Masuk</button>
		  </form>
		</main>
		

		<script src="<?=base_url('_vendors/plugins/jquery/jquery.min.js')?>"></script>
		<script src="<?=base_url('_vendors/plugins/bootstrap/js/bootstrap.min.js')?>"></script>
	</body>
</html>