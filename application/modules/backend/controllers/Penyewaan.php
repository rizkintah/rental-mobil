<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penyewaan extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!is_logged_admin()) redirect('admin');
		$this->load->model('Penyewaan_model');
	}

	function get_penyewaan()
	{
		$data['title'] = 'List Penyewaan';
		theme_backend('penyewaan/index', $data);
	}

	function get_list_penyewaan()
	{
		if($this->input->is_ajax_request()) {
      $filter = json_decode($this->input->get('filter'), true);
      $limit = $this->input->get('limit');
      $offset = $this->input->get('offset');
      $sort = $this->input->get('sort');
      $order = $this->input->get('order');
      $limit2 = $offset + $limit;

      $tipe = $this->input->get('tipe');
      $q    = $this->input->get('q');
      $statusSewa  = $this->input->get('kat');
      $search = null;

      if(!empty($tipe) && !empty($q)) $search = "AND LOWER({$tipe}) LIKE LOWER('%{$q}%')";

      $data = $this->Penyewaan_model->get_list_penyewaan($filter, $offset, $limit2, $sort, $order, $search, $statusSewa);
      echo json_encode($data);
    }else{
      echo "<pre>";
      print_r ("no allowed access");
      echo "</pre>";
    }
	}

	function get_verifikasi() {

		$jenisTransaksi = $this->uri->segment(3);
		$pid 						= $this->uri->segment(4);
		$mid 						= $this->input->get('mid');
		$jamMulaiSewa		= date('H:i', strtotime($this->input->get('jam_sewa')));
		$jamSelesaiSewa	= date('H:i', strtotime($this->input->get('jam_sewa')));
		
		/*
		* update status sesuai jenis transakasi 
		* ex: misal transaksi verifikasi pembayaran maka update status menjadi 6 (Kendaraan Siap Diambil)
		*/
		$arrStatus = array(
			'pembayaran'=> array('status_sewa' => 6),
			'penyewa' 	=> array(
				'status_sewa' => 1, 
				'waktu_mulai_sewa' => $jamMulaiSewa, 
				'waktu_kembali' => $jamSelesaiSewa
			),
			'selesai' 	=> array(
				'status_sewa' => 2,
				'tanggal_dikembalikan' => date('Y-m-d H:i')
			),
			'batal' 		=> array('status_sewa' => 3),
		);

		# optional data
		$options = array(
			'jenisTransaksi' => $jenisTransaksi,
			'mid'	=> $mid
		);

		$upd = $this->Penyewaan_model->updateStatusPenyewaan($pid, $arrStatus[$jenisTransaksi], $options);
		if ($upd) {
			$this->session->set_flashdata('notif', 'Transaksi berhasil diupdate');
		}else{
			$this->session->set_flashdata('notif', 'Transaksi gagal diupdate');
		}

		redirect('admin/penyewaan');
	}

}

/* End of file Penyewaan.php */
/* Location: ./application/modules/backend/controllers/Penyewaan.php */