<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!is_logged_admin()) redirect('admin');
		$this->load->model('Users_model');
	}

	public function index()
	{
		$data['title'] = 'Manajemen Pengguna';
		theme_backend('users/index', $data);
	}

	function get_list_users() {
    if($this->input->is_ajax_request()) {
      $filter = json_decode($this->input->get('filter'), true);
      $limit = $this->input->get('limit');
      $offset = $this->input->get('offset');
      $sort = $this->input->get('sort');
      $order = $this->input->get('order');
      $limit2 = $offset + $limit;

      $tipe = $this->input->get('tipe');
      $q    = $this->input->get('q');
      $search = null;

      if(!empty($tipe) && !empty($q)) $search = "AND LOWER({$tipe}) LIKE LOWER('%{$q}%')";

      $data = $this->Users_model->getListUsers($filter, $offset, $limit2, $sort, $order, $search);
      
      echo json_encode($data);
    }else{
      echo "<pre>";
      print_r ("no allowed access");
      echo "</pre>";
    }
  }

  function add_users()
  {
  	$this->form_validation->set_rules('nama_pengguna', 'Nama Pengguna', 'trim|required');
  	$this->form_validation->set_rules('email', 'Email Pengguna', 'trim|required|valid_email');
  	$this->form_validation->set_rules('username', 'Username', 'trim|required|callback_check_user');
  	$this->form_validation->set_rules('password', 'Password', 'trim|required');
  	$this->form_validation->set_rules('groupid', 'Tipe User', 'trim|required');

  	if ($this->form_validation->run($this) == TRUE) {
  		# code...
	  		$arrInsert = array(
	  			'group_id'			=> $this->input->post('groupid'),
					'nama_pengguna' => $this->input->post('nama_pengguna'),
					'email' 				=> $this->input->post('email'),
					'username' 			=> $this->input->post('username'),
					'password' 			=> password_hash($_POST['password'], PASSWORD_DEFAULT),
	  		);

	  		$insert = $this->Users_model->insertUser($arrInsert);
	  		if($insert) {
	  			$this->session->set_flashdata('notif', 'Data Pengguna berhasil ditambah');
					redirect('admin/users');
	  		}else{
	  			$this->session->set_flashdata('notif', 'Data Pengguna gagal ditambah');
					redirect('admin/users');
	  		}
  	} else {

  		# code...
	  		$data['title'] = 'Tambah Data Pengguna';
		  	$data['listGroup'] = $this->Users_model->getListGroup();
				theme_backend('users/form', $data);
  	}
  }


  function update_users($userID) {
  	$this->form_validation->set_rules('groupid', 'Tipe User', 'trim|required');
  	$this->form_validation->set_rules('nama_pengguna', 'Nama Pengguna', 'trim|required');
  	$this->form_validation->set_rules('no_identitas', 'No. Identitas', 'trim|required');
  	$this->form_validation->set_rules('no_hp', 'No. Handphone', 'trim|required|is_numeric');
  	$this->form_validation->set_rules('email', 'Email Pengguna', 'trim|required|valid_email');
  	$this->form_validation->set_rules('username', 'Username', 'trim|required|callback_check_user');
  	$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');

  	if ($this->form_validation->run($this) == TRUE) {
  		# code...
  			$arrUpdate = array(
	  			'group_id'			=> $this->input->post('groupid'),
					'nama_pengguna' => $this->input->post('nama_pengguna'),
					'email' 				=> $this->input->post('email'),
					'username' 			=> $this->input->post('username'),
					'password' 			=> $this->input->post('password'),
					'no_identitas' 	=> $this->input->post('no_identitas'),
					'alamat' 				=> $this->input->post('alamat'),
	  		);

	  		if (!empty($_FILES['file_ktp']['name'])) {
	  			$uploadKtp = upload_single_file('./_files/_ktp', 'file_ktp', '*');
					$arrUpdate['file_ktp'] = $uploadKtp['file_name'];
	  		}

  			if(!empty($_POST['password'])) $arrUpdate['password'] = password_hash($_POST['password'], PASSWORD_DEFAULT);

	  		$update = $this->Users_model->updateUser(decryptUrl($_POST['id']), $arrUpdate);
	  		if($update) {
	  			$this->session->set_flashdata('notif', 'Data Pengguna berhasil diubah');
					redirect('admin/users');
	  		}else{
	  			$this->session->set_flashdata('notif', 'Data Pengguna gagal diubah');
					redirect('admin/users');
	  		}
  	} else {
  		# code...
  			$id = decryptUrl($userID);
			  $data['title'] = 'Update Data Pengguna';
		  	$data['detail'] = $this->Users_model->getDetailUserBy($id);
		  	$data['listGroup'] = $this->Users_model->getListGroup();
				theme_backend('users/form', $data);
  	}
  }

  function delete_users($userID) {
  	$id = decryptUrl($userID);
  	$del = $this->Users_model->deleteUser($id);
  	if($del) {
			$this->session->set_flashdata('notif', 'Data Pengguna berhasil dihapus');
			redirect('admin/users');
		}else{
			$this->session->set_flashdata('notif', 'Data Pengguna gagal dihapus');
			redirect('admin/users');
		}
  }


  function check_user($str)
	{
		$id = null;
		if($this->input->post('id')) $id = decryptUrl($this->input->post('id'));

		$check = $this->Users_model->checkUsernameExisting($str, $id);
		if (!$check) {
			$this->form_validation->set_message('check_user', 'The {field} is already taken');
    	return FALSE;
		}else{
			return TRUE;
		}
	}

}

/* End of file Users.php */
/* Location: ./application/modules/backend/controllers/Users.php */