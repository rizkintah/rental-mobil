<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!is_logged_admin()) redirect('admin');
	}

	public function index()
	{
		
	}

	function dashboard() {
		$data['title'] = 'dashboard';
		theme_backend('dashboard/index', $data);
	}

}

/* End of file Backend.php */
/* Location: ./application/modules/backend/controllers/Backend.php */