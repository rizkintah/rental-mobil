<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!is_logged_admin()) redirect('admin');
		$this->load->model('Laporan_model');
	}

	public function penyewaan()
	{
		$data['filter']  = $this->input->post('filter') ? $this->input->post('filter') : 'daterange';
		$data['starDate'] = $this->input->post('sd') ? reverseDate($this->input->post('sd'), "-") : date('Y-m-01');
		$data['endDate'] 	= $this->input->post('en') ? reverseDate($this->input->post('en'), "-") : date('Y-m-d');
		$data['pengguna'] 	= $this->input->post('pengguna') ? $this->input->post('pengguna') : '';
		$data['title'] = 'Laporan Penyewaan';

		$data['listData'] = $this->Laporan_model->getLaporanPenyewaan($data['starDate'], $data['endDate'], $data['filter'], $data['pengguna']);
		theme_backend('laporan/penyewaan', $data);
	}

	function get_list_penyewa_by()
	{
		if ($this->input->is_ajax_request()) {
			$keyword = $this->input->get('keyword');
			$res = $this->Laporan_model->getListPenyewaBy($keyword);
			echo json_encode($res);
		}else{
			redirect(site_url());
		}
	}

}

/* End of file Laporan.php */
/* Location: ./application/modules/backend/controllers/Laporan.php */