<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mobil extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!is_logged_admin()) redirect('admin');
		$this->load->model('Mobil_model');
		$this->load->model('Jenis_mobil');
	}

	# manajemen mobil
		public function index()
		{
			$data['title'] = 'Data Mobil';
			theme_backend('mobil/index', $data);
		}

		function get_list_mobil() {
	    if($this->input->is_ajax_request()) {
	      $filter = json_decode($this->input->get('filter'), true);
	      $limit = $this->input->get('limit');
	      $offset = $this->input->get('offset');
	      $sort = $this->input->get('sort');
	      $order = $this->input->get('order');
	      $limit2 = $offset + $limit;

	      $tipe = $this->input->get('tipe');
	      $q    = $this->input->get('q');
	      $search = null;

	      if(!empty($tipe) && !empty($q)) $search = "AND LOWER({$tipe}) LIKE LOWER('%{$q}%')";

	      $data = $this->Mobil_model->getListMobil($filter, $offset, $limit2, $sort, $order, $search);
	      
	      echo json_encode($data);
	    }else{
	      echo "<pre>";
	      print_r ("no allowed access");
	      echo "</pre>";
	    }
	  }

		function add_mobil()
		{

			$this->form_validation->set_rules('merk', 'Merk', 'trim|required');
			$this->form_validation->set_rules('jenis', 'Jenis Mobil', 'trim|required');
			$this->form_validation->set_rules('warna', 'Warna Mobil', 'trim|required');
			$this->form_validation->set_rules('nopol', 'Nomor Polisi Mobil', 'trim|required');
			$this->form_validation->set_rules('harga_sewa', 'Harga Sewa Mobil', 'trim|required|numeric');

			if ($this->form_validation->run($this) == TRUE) {
				$arrData = array(
					'jenis_id' 		=> $this->input->post('jenis'),
					'warna' 			=> $this->input->post('warna'),
					'no_polisi' 	=> $this->input->post('nopol'),
					'harga_sewa' 	=> $this->input->post('harga_sewa'),
					'inserted_by' => $this->session->userdata('userID'),
				);
				
				$insertMobil = $this->Mobil_model->insertMobilToDB($arrData);

				if (!empty($insertMobil)) {
					if (!empty($_FILES['imgMobil']['name'])) {
						$imgArr = upload_multiple_files('./_files/_mobil/', $_FILES['imgMobil'], '*');
						$insertImg = $this->Mobil_model->insertImgMobil($insertMobil, $imgArr);
					}
					$this->session->set_flashdata('notif', 'Data mobil berhasil ditambah');
					redirect('admin/mobil');
				}else{
					$this->session->set_flashdata('notif', 'Data mobil gagal ditambah');
					redirect('admin/mobil');
				}
			} else {
				$data['title'] = 'Tambah Data Mobil';
				$data['merkMobil'] = $this->Mobil_model->getListMerkMobil();
				theme_backend('mobil/form', $data);
			}
		}

		function update_mobil($mobilID)
		{

			$this->form_validation->set_rules('merk', 'Merk', 'trim|required');
			$this->form_validation->set_rules('jenis', 'Jenis Mobil', 'trim|required');
			$this->form_validation->set_rules('warna', 'Warna Mobil', 'trim|required');
			$this->form_validation->set_rules('nopol', 'Nomor Polisi Mobil', 'trim|required');
			$this->form_validation->set_rules('harga_sewa', 'Harga Sewa Mobil', 'trim|required|numeric');

			if ($this->form_validation->run($this) == TRUE) {

				$arrData = array(
					'jenis_id' 		=> $this->input->post('jenis'),
					'warna' 			=> $this->input->post('warna'),
					'no_polisi' 	=> $this->input->post('nopol'),
					'harga_sewa' 	=> $this->input->post('harga_sewa'),
					'inserted_by' => $this->session->userdata('userID'),
				);

				$arrImgDel = $this->input->post('imgMobilDelete');
				$delArrImg = $this->Mobil_model->deleteMobilImageIn($arrImgDel, decryptUrl($mobilID));
				
				$update = $this->Mobil_model->updateMobilToDB(decryptUrl($mobilID), $arrData);

				if ($update) {
					if (!empty($_FILES['imgMobil']['name'])) {
						$imgArr = upload_multiple_files('./_files/_mobil/', $_FILES['imgMobil'], '*');
						$insertImg = $this->Mobil_model->insertImgMobil(decryptUrl($mobilID), $imgArr);
					}
					$this->session->set_flashdata('notif', 'Data mobil berhasil diupdate');
					redirect('admin/mobil');
				}else{
					$this->session->set_flashdata('notif', 'Data mobil gagal diupdate');
					redirect('admin/mobil');
				}
			} else {
				$data['title'] = 'Update Data Mobil';
				$data['detail'] = $this->Mobil_model->getDetailMobilByID(decryptUrl($mobilID));
				$data['listImage'] = $this->Mobil_model->getMobilImages(decryptUrl($mobilID));
				$data['merkMobil'] = $this->Mobil_model->getListMerkMobil();
				theme_backend('mobil/form', $data);
			}
		}

		function delete_mobil($mobilID)
		{
			$detailMobil = $this->Mobil_model->getDetailMobilByID(decryptUrl($mobilID));
			if ($detailMobil['status'] == 'tidak tersedia') {
				$this->session->set_flashdata('notif', 'Mobil gagal dihapus disebabkan status mobil sedang disewa.');
				redirect('admin/mobil');
			}else{
				$del = $this->Mobil_model->deleteMobilFromDB(decryptUrl($mobilID));
				if ($del) {
					$this->session->set_flashdata('notif', 'Mobil berhasil dihapus');
					redirect('admin/mobil');
				}else{
					$this->session->set_flashdata('notif', 'Mobil gagal dihapus');
					redirect('admin/mobil');
				}
			}

		}

		function get_list_jenis_by_merk() {
			if ($this->input->is_ajax_request()) {
				$id 					 = $this->input->get('merk_id');
				$data['curr_jenis'] 	= $this->input->get('curr_jenis');
				$data['lists'] = $this->Jenis_mobil->getListListByMerkID($id);
				$this->load->view('mobil/list_jenis', $data);
			}
		}
	# end Data mobil

	# manajemen merk mobil
		function merk()
		{
			$data['title'] 	= 'Data Merk Mobil';
			$data['lists']	= $this->Mobil_model->getListMerkMobil();
			theme_backend('mobil/merk/index', $data);
		}

		function add_merk()
		{
			$this->form_validation->set_rules('merk', 'Merk Mobil', 'trim|required|callback_check_merk');
			$this->form_validation->set_rules('merk_icon', 'Merk Icon', 'callback_ext_merk_icon');
			if ($this->form_validation->run($this) == TRUE) {
				$imgUpload = upload_single_file('./_files/_merk/', 'merk_icon', 'jpg|png|gif|jpeg|JPEG|PNG|JPG|GIF');
				if ($imgUpload['error'] == 'no') {
					$_POST['merk_icon'] = $imgUpload['file_name'];
					$insrt = $this->_process_add_merk($_POST);
					if ($insrt) {
						$this->session->set_flashdata('notif', 'Merk mobil berhasil ditambah');
						redirect('admin/mobil/merk');
					}else{
						$this->session->set_flashdata('notif', 'Merk mobil gagal ditambah');
						redirect('admin/mobil/merk/add');
					}
				}else{
					$this->session->set_flashdata('notif', $imgUpload['error']);
					redirect('admin/mobil/merk/add');
				}
			} else {
				# code
					$data['title'] = 'Tambah Data Merk Mobil';
					theme_backend('mobil/merk/add', $data);
			}
		}

		function ext_merk_icon() {
			// ini utk mengambil apakah proses input atau update
			$uriProses = $this->uri->segment(4);
			$path = $_FILES['merk_icon']['name'];
			$arrExt = array('jpg','png','jpeg','gif');
			$ext = pathinfo($path, PATHINFO_EXTENSION);
			
			if($uriProses == 'update') { // jika proses update
				if(empty($_FILES['merk_icon']['name'])) { 
					return TRUE;
				}else{
					if(!in_array(strtolower($ext), $arrExt)) {
						$this->form_validation->set_message('ext_merk_icon', $path.' file Extension not allowed');
	      		return FALSE;
					}else{
						return TRUE;
					}
				}
			}else{ // jika proses insert
				if(empty($_FILES['merk_icon']['name'])) { 
					$this->form_validation->set_message('ext_merk_icon', 'The Merk Icon field is required.');
	      	return FALSE;
				}elseif(!in_array(strtolower($ext), $arrExt)) {
					$this->form_validation->set_message('ext_merk_icon', $path.' File Extension not allowed');
	      	return FALSE;
				}else{
					return TRUE;
				}
			}
		}

		function check_merk($str)
		{
			$id = null;
			if($this->input->post('id')) $id = decryptUrl($this->input->post('id'));

			$check = $this->Mobil_model->checkMerkMobilExisting($str, $id);
			if (!$check) {
				$this->form_validation->set_message('check_merk', 'The {field} is already taken');
      	return FALSE;
			}else{
				return TRUE;
			}
		}

		function _process_add_merk($data) {
			$arrInsert = array(
				'merk_nama' => $data['merk'],
				'merk_icon' => $data['merk_icon'],
				'added_by' 	=> $this->session->userdata('userID'),
			);

			$insert = $this->Mobil_model->insertMerkMobil($arrInsert);
			if ($insert) {
				return true;
			}else{
				return false;
			}
		}

		function update_merk($id)
		{
			$this->form_validation->set_rules('merk', 'Merk Mobil', 'trim|required|callback_check_merk');
			$this->form_validation->set_rules('merk_icon', 'Merk Icon', 'callback_ext_merk_icon');
			
			if ($this->form_validation->run($this) == TRUE) {
				$imgUpload = null;
				// check apakah image di update
				if(!empty($_FILES['merk_icon']['name'])) 
					$imgUpload = upload_single_file('./_files/_merk/', 'merk_icon', 'jpg|png|gif|jpeg|JPEG|PNG|JPG|GIF');

				if (empty($imgUpload) || ($imgUpload['error'] == 'no')) {
					$_POST['merk_icon'] = $imgUpload  ? $imgUpload['file_name'] : null;
					$upd = $this->_process_update_merk($_POST);
					if ($upd) {
						$this->session->set_flashdata('notif', 'Merk mobil berhasil di update');
						redirect('admin/mobil/merk');
					}else{
						$this->session->set_flashdata('notif', 'Merk mobil gagal di update');
						redirect('admin/mobil/merk/update/'.$id);
					}
				}else{
					$this->session->set_flashdata('notif', $imgUpload['error']);
					redirect('admin/mobil/merk/update/'.$id);
				}
			} else {
				# code
					$data['detail'] = $this->Mobil_model->getDetailMerkMobilByID(decryptUrl($id));
					$data['title'] 	= 'Update Data Merk Mobil';
					theme_backend('mobil/merk/update', $data);
			}
		}

		function _process_update_merk($data) {
			$arrInsert = array(
				'merk_nama' => $data['merk'],
				'added_by' 	=> $this->session->userdata('userID'),
			);

			$data['merk_icon'] ? $arrInsert['merk_icon'] = $data['merk_icon'] : null;


			$upd = $this->Mobil_model->updateMerkMobil(decryptUrl($data['id']), $arrInsert);
			if ($upd) {
				return true;
			}else{
				return false;
			}
		}

		function delete_merk($id)
		{
			$id = decryptUrl($id);
			$detail = $this->Mobil_model->getDetailMerkMobilByID($id);
			$delFile = unlink(getcwd().'/_files/_merk/'.$detail['merk_icon']);
			if ($delFile) {
				$del = $this->Mobil_model->deleteMerkMobilByID($id);
				$this->session->set_flashdata('notif', 'Merk mobil berhasil di delete');
				redirect('admin/mobil/merk');
			}else{
				$this->session->set_flashdata('notif', 'Merk mobil gagal di delete');
				redirect('admin/mobil/merk');
			}
		}
	# end manajemen merk mobil

	# manajemen jenis mobil
		function jenis() {
			$data['title'] = 'Data Tipe Mobil';
			$data['listJenis'] = $this->Jenis_mobil->getLists();
			theme_backend('mobil/jenis/index', $data);
		}

		function add_jenis() {
			$this->form_validation->set_rules('jenis', 'Tipe Mobil', 'trim|required|callback_check_duplicate_jenis');
			$this->form_validation->set_rules('merk', 'Merk Mobil', 'trim|required');
			
			if ($this->form_validation->run($this) == TRUE) {
				# create array for insert to db
					$arrData = array(
						'nama_jenis' => $this->input->post('jenis'),
						'merk_id' 	 => $this->input->post('merk'),
					);

				$insert = $this->Jenis_mobil->insertToDB($arrData);
				if ($insert) {
					$this->session->set_flashdata('notif', 'Tipe mobil berhasil ditambah');
					redirect('admin/mobil/jenis');
				}else{
					$this->session->set_flashdata('notif', 'Tipe mobil gagal ditambah');
					redirect('admin/mobil/jenis/add');
				}
			}else{
				$data['title'] = 'Tambah Data Tipe Mobil';
				$data['merkMobil'] = $this->Mobil_model->getListMerkMobil();
				theme_backend('mobil/jenis/form', $data);
			}
		}

		function check_duplicate_jenis($str)
		{
			$id = null;
			if($this->input->post('id')) $id = decryptUrl($this->input->post('id'));

			$check = $this->Jenis_mobil->checkJenisDuplicate($str, $id);
			if (!$check) {
				$this->form_validation->set_message('check_duplicate_jenis', 'The {field} is already taken');
      	return FALSE;
			}else{
				return TRUE;
			}
		}

		function update_jenis() {
			$this->form_validation->set_rules('jenis', 'Tipe Mobil', 'trim|required|callback_check_duplicate_jenis');
			$this->form_validation->set_rules('merk', 'Merk Mobil', 'trim|required');

			if ($this->form_validation->run($this) == TRUE) {
				$id = decryptUrl($this->input->post('id'));
				# create array for insert to db
					$arrData = array(
						'nama_jenis' => $this->input->post('jenis'),
						'merk_id' 	 => $this->input->post('merk'),
					);

				$update = $this->Jenis_mobil->updateToDB($id, $arrData);
				if ($update) {
					$this->session->set_flashdata('notif', 'Tipe mobil berhasil diupdate');
					redirect('admin/mobil/jenis');
				}else{
					$this->session->set_flashdata('notif', 'Tipe mobil gagal diupdate');
					redirect('admin/mobil/jenis/update');
				}
			} else {
				$id = decryptUrl($this->uri->segment(5));
				$data['title'] = 'Update Data Tipe Mobil';
				$data['detail'] = $this->Jenis_mobil->getDetailByID($id);
				$data['merkMobil'] = $this->Mobil_model->getListMerkMobil();
				theme_backend('mobil/jenis/form', $data);
			}
		}

		function delete_jenis($id) {
			$id = decryptUrl($id);
			$del = $this->Jenis_mobil->deleteFromDB($id);
			if ($del) {
				$this->session->set_flashdata('notif', 'Tipe mobil berhasil di delete');
				redirect('admin/mobil/jenis');
			}else{
				$this->session->set_flashdata('notif', 'Tipe mobil gagal di delete');
				redirect('admin/mobil/jenis');
			}
		}
	# end manajemen jenis mobil
}

/* End of file Mobil.php */
/* Location: ./application/modules/backend/controllers/Mobil.php */