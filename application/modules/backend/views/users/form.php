<?php  
  $groupExisting  = isset($detail['group_id']) ? $detail['group_id'] : null;
  $group_id = !empty($_POST['groupid']) ? $_POST['groupid'] : $groupExisting;
  // $jenis_id = isset($detail['jenis_id']) ? $detail['jenis_id'] : null;
  $act = $this->uri->segment(3);
?>

<div class="card border border-white shadow-sm mt-3">
  <div class="card-header bg-white">
    <strong><?=$title?></strong>
  </div>
  <div class="card-body">
    <form action="<?=current_url()?>" method="POST" enctype="multipart/form-data">
      <?php if ($act == 'update'): ?>
        <input type="hidden" name="id" id="user_id" value="<?=encryptUrl($detail['pengguna_id'])?>">
      <?php endif ?>
      
      <div class="row mb-3">
        <label for="itipe" class="col-sm-2 col-form-label">Tipe User</label>
        <div class="col-sm-10">
          <select name="groupid" id="itipe" class="form-control <?=form_error('groupid') ? 'is-invalid' : null?>" >
            <option value="" disabled
                    <?=empty($_POST['groupid']) ? 'selected' : null;?>>
                  Pilih tipe user
            </option>
            <?php foreach ($listGroup as $k => $v): ?>
              <option value="<?=$v['group_id']?>" 
                <?php echo set_select('groupid', $group_id, $group_id == $v['group_id']); ?>
              >
                <?=ucwords($v['nama_group'])?>
              </option>
            <?php endforeach ?>
          </select>
          <?php echo form_error('groupid','<div class="invalid-feedback">', '</div>'); ?>
        </div>
      </div>

      <div class="row mb-3">
        <label for="inama" class="col-sm-2 col-form-label">Nama Pengguna</label>
        <div class="col-sm-10">
          <input type="text" name="nama_pengguna" id="inama" 
                 class="form-control <?=form_error('nama_pengguna') ? 'is-invalid' : null?>"
                 value="<?=set_value('nama_pengguna', isset($detail['nama_pengguna']) ? $detail['nama_pengguna'] : null)?>">
          <?php echo form_error('nama_pengguna','<div class="invalid-feedback">', '</div>'); ?>
        </div>
      </div>

      <?php if ($act == 'update'): ?>
        <div class="row mb-3">
          <label for="iidentitas" class="col-sm-2 col-form-label">No. Identitas</label>
          <div class="col-sm-10">
             <input type="text" name="no_identitas" id="iidentitas" 
                   class="form-control <?=form_error('no_identitas') ? 'is-invalid' : null?>"
                   value="<?=set_value('no_identitas', isset($detail['no_identitas']) ? $detail['no_identitas'] : null)?>">
              
              <?php echo form_error('no_identitas','<div class="invalid-feedback">', '</div>'); ?>
          </div>
        </div>

        <div class="row mb-3">
          <label for="filektp" class="col-sm-2 col-form-label">File KTP</label>
          <div class="col-sm-10">
            <div class="input-group mb-1">
              <input type="file" name="file_ktp" class="form-control" id="filektp">
              <button class="btn btn-outline-secondary btn-password" type="button" 
                      data-bs-toggle="modal" 
                      data-bs-target="#exampleModal">Lihat KTP</button>

              <?php echo form_error('no_identitas','<div class="invalid-feedback">', '</div>'); ?>
            </div>
            <small class="mt-5 text-success">
              Upload ulang <strong>File KTP</strong> jika melakukan perubahan pada <strong>No. Identitas</strong>
            </small>

          </div>
        </div>

        <div class="row mb-3">
          <label for="ihp" class="col-sm-2 col-form-label">No. Handphone</label>
          <div class="col-sm-10">
            <input type="text" name="no_hp" id="ihp" 
                   class="form-control <?=form_error('no_hp') ? 'is-invalid' : null?>"
                   value="<?=set_value('no_hp', isset($detail['no_hp']) ? $detail['no_hp'] : null)?>">
            <?php echo form_error('no_hp','<div class="invalid-feedback">', '</div>'); ?>
          </div>
        </div>
      <?php endif ?>

      <div class="row mb-3">
        <label for="iemail" class="col-sm-2 col-form-label">Email Pengguna</label>
        <div class="col-sm-10">
          <input type="email" name="email" id="iemail" 
                 class="form-control <?=form_error('email') ? 'is-invalid' : null?>"
                 value="<?=set_value('email', isset($detail['email']) ? $detail['email'] : null)?>">
          <?php echo form_error('email','<div class="invalid-feedback">', '</div>'); ?>
        </div>
      </div>

      <div class="row mb-3">
        <label for="iuser" class="col-sm-2 col-form-label">Username</label>
        <div class="col-sm-10">
          <input type="text" name="username" id="iuser" 
                 class="form-control <?=form_error('username') ? 'is-invalid' : null?>"
                 value="<?=set_value('username', isset($detail['username']) ? $detail['username'] : null)?>">
          <?php echo form_error('username','<div class="invalid-feedback">', '</div>'); ?>
        </div>
      </div>

      <div class="row mb-3">
        <label for="ipassword" class="col-sm-2 col-form-label">Password</label>
        <div class="col-sm-10">
          <div class="input-group">
            <input type="password" name="password" id="ipassword" 
                 class="form-control <?=form_error('password') ? 'is-invalid' : null?> password"
                 value="<?=set_value('password')?>">
            <button class="btn btn-outline-secondary btn-password" type="button" onclick="showPassword($(this));">Lihat Password</button>
          </div>
          
          <?php echo form_error('password','<div class="invalid-feedback">', '</div>'); ?>
        </div>
      </div>

      <?php if ($act == 'update'): ?>
        <div class="row mb-3">
          <label for="ialamat" class="col-sm-2 col-form-label">Alamat</label>
          <div class="col-sm-10">
            <textarea name="alamat" 
                      id="ialamat" 
                      rows="2"  style="resize:none;"
                      class="form-control <?=form_error('password') ? 'is-invalid' : null?>"><?=set_value('alamat', isset($detail['alamat']) ? $detail['alamat'] : null)?></textarea>
            
            <?php echo form_error('password','<div class="invalid-feedback">', '</div>'); ?>
          </div>
        </div>
      <?php endif ?>

      <hr>

      <div class="col-sm-10 offset-sm-2">
        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>">
        <a href="<?=site_url('admin/users')?>" class="btn btn-outline-danger">
          Kembali
        </a>
        <button type="submit" class="btn btn-dark ms-1">
          Simpan Data
        </button>
      </div>

    </form>
  </div>
</div>



<!-- Modal -->
<?php if ($act == 'update'): ?>
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">File KTP</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <img src="<?=base_url('_files/_ktp/'.$detail['file_ktp'])?>" class="img-fluid" alt="file ktp">
        </div>
      </div>
    </div>
  </div>
<?php endif ?>