<div class="card border border-white shadow-sm mt-3">
  <div class="card-header bg-white">
    <span data-feather="users" class="feather-16 me-1"></span> <?=$title?>
  </div>
  <div class="card-body">
    <div id="toolbar">
      <div class="row gy-2 gx-3 align-items-center">
        <div class="col-auto">
          <select name="tipe" class="form-select form-select-sm">
            <option selected>Cari Berdasarkan</option>
            <option value="nama_pengguna">Nama Pengguna</option>
            <option value="no_hp">No. Handphone</option>
            <option value="no_identitas">No. Identitas</option>
            <option value="status">Status</option>
            <option value="nama_group">Tipe User</option>
          </select>
        </div>

        <div class="col-auto">
          <input type="search" name="q" class="form-control form-control-sm" placeholder="Kata kunci">
        </div>

        <div class="col-auto">
          <button type="submit" class="btn btn-info btn-sm" id="btnCari">
            <span data-feather="search" class="feather-16"></span> Cari
          </button>
        </div>
      </div>
    </div>
    <table  data-toggle="table"
            id="tblUsers"
            data-buttons="btnAdd"
            data-show-button-text="true"
            data-buttons-prefix="btn btn-sm btn-primary"
            data-toolbar="#toolbar"
            data-pagination="true"
            data-filter-control="true"  
            --data-cell-style="cellStyle"
            data-only-info-pagination="false"
            data-query-params="qParams"
            data-response-handler="responseHandler"
            --data-show-refresh="true"
            data-side-pagination="server" 
            data-page-list="[10,30,40,50,60,100,200,300,1000]" 
            data-page-size="10"
            data-sortable="true"
            data-url="<?=site_url('admin/users/lists')?>"
            class="table-striped table-hover bstable text-nowrap">
      <thead>
        <tr>
          <th data-field="nama_pengguna" data-halign="center" data-align="center" data-valign="middle">Nama Pengguna</th>
          <th data-field="no_hp" data-halign="center" data-align="center" data-valign="middle">No. Handphone</th>
          <th data-field="no_identitas" data-halign="center" data-align="center" data-valign="middle">No. Identitas</th>
          <th data-field="alamat" data-halign="center" data-align="center" data-valign="middle">Alamat</th>
          <th data-field="email" data-halign="center" data-align="center" data-valign="middle">Email</th>
          <th data-field="username" data-halign="center" data-align="center" data-valign="middle">Username</th>
          <th data-field="status" data-halign="center" data-align="center" data-valign="middle">Status</th>
          <th data-field="tanggal_registrasi" data-halign="center" data-align="center" data-valign="middle">Tanggal Registrasi</th>
          <th data-field="nama_group" data-halign="center" data-align="center" data-valign="middle">Tipe User</th>
          <th data-field="pengguna_id" data-halign="center" data-align="center" data-formatter="btnActionUsers" data-valign="middle">#</th>
      </thead>
      
    </table>

  </div>
</div>