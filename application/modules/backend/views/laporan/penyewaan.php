<div class="card border border-white shadow-sm mt-3">
  <div class="card-header bg-white">
    <span data-feather="file" class="feather-16 me-1"></span> <?=$title?>
  </div>
  <div class="card-body">
    <div class="row mb-3">
      <div class="col-md-10">
        <form action="<?=current_url()?>" method="POST" class="row g-1">
          <div class="col-md-3">
            <select name="filter" id="selectFilterBy" class="form-control form-control-sm">
              <option value="">Filter berdasarkan</option>
              <option value="daterange" <?=$filter == 'daterange' ? 'selected' : null?>>
                Periode Penyewaan
              </option>
              <option value="pengguna" <?=$filter == 'pengguna' ? 'selected' : null?>>
                Nama Penyewa
              </option>
            </select>
          </div>
          
          <div class="col-md-5">
            <div id="pengguna" class="d-none">
              <input type="text" name="pengguna" id="penggunaInput" class="form-control form-control-sm" required="required" placeholder="Ketikan nama penyewa..." value="<?=$pengguna?>">
            </div>
            <div class="input-group input-group-sm d-none" id="daterange">
              <input type="text" name="sd" id="sd"  data-target="sd" data-toggle="datetimepicker" class="form-control datetimepicker-input" placeholder="Tanggal Mulai" value="<?=date('d-m-Y', strtotime($starDate))?>">
              <span class="input-group-text">-</span>
              <input type="text" name="ed" id="ed" data-target="ed" data-toggle="datetimepicker" class="form-control datetimepicker-input" placeholder="Tanggal Selesai" value="<?=date('d-m-Y', strtotime($endDate))?>">
            </div>
          </div>
          <div class="col-md-3">
            <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>">
            <button type="submit" class="btn btn-info btn-sm ms-1" id="btnCari">
              <span data-feather="search" class="feather-16"></span> Submit
            </button>
          </div>
        </form>
      </div>

      <div class="col-md-2 text-end">
          <button type="button" 
                  class="btn btn-sm btn-success btn-prints  <?=!empty($listData) ? null : 'disabled'?>"
                  onclick="printJS('printable', 'html')">
            Print Laporan
          </button>
      </div>
    </div>

    <div id="printable">
      <div class="visually-hidden header-label">
        <div>
          <img src="<?=base_url('_vendors/themes/images/logo.jpeg')?>" alt="logo" width="80">
        </div>

        <div align="center">
          <p class="primary-header">PT. TRIKARYA SEJATI</p>
          <p class="secondary-header">
            LAPORAN DATA SEWA KENDARAAN <br>
            <?php if ($filter == 'daterange'): ?>
              PERIODE <?=date('d M Y', strtotime($starDate))?> - <?=date('d M Y', strtotime($endDate))?>
            <?php else: ?>
              Atas Nama <?=$pengguna?>
            <?php endif ?>
          </p>
        </div>
        <br>
      </div>

      <table class="table table-bordered table-striped table-hover bstable text-nowrap">
        <thead>
          <tr>
            <th class="text-center">No</th>
            <th class="text-center">Tanggal Sewa</th>
            <th class="text-center">No. Transaksi</th>
            <th class="text-center">Nama Penyewa</th>
            <th class="text-center">Merk Mobil</th>
            <th class="text-center">Tipe Mobil</th>
            <th class="text-center">No. Polisi</th>
            <th class="text-center">Nilai Sewa</th>
          </tr>
        </thead>

        <?php if (!empty($listData)): ?>
          <tbody>
          	<?php foreach ($listData as $k => $v): ?>
          		<tr>
          			<td class="text-center" align="center"><?=$k+1?></td>
          			<td class="text-center" align="center"><?=date('d M Y', strtotime($v['tanggal_sewa']))?></td>
          			<td class="text-center" align="center"><?=$v['penyewaan_id']?></td>
          			<td class="text-center" align="center"><?=$v['nama_pengguna']?></td>
          			<td class="text-center" align="center"><?=$v['merk_nama']?></td>
          			<td class="text-center" align="center"><?=$v['nama_jenis']?></td>
          			<td class="text-center" align="center"><?=$v['no_polisi']?></td>
          			<td class="text-end" align="right"><?=number_format($v['total_harga_sewa'])?></td>
          		</tr>
          	<?php endforeach ?>
          </tbody>
          <tfoot>
          	<tr>
          		<td colspan="7" class="text-center fw-bolder" align="center"><strong>Total</strong></td>
          		<td class="text-end fw-bolder" align="right">
          			<strong><?=number_format(array_sum(array_column($listData, 'total_harga_sewa')))?></strong>
          		</td>
          	</tr>
          </tfoot>
        <?php else: ?>
          <tbody>
            <tr>
              <td colspan="8" class="text-center fw-bolder">
                <?php if ($filter == "daterange"): ?>
                  Tidak ada laporan penyewaan pada range tanggal <?=date('d M Y', strtotime($starDate))?> - <?=date('d M Y', strtotime($endDate))?>
                <?php else: ?>
                  Tidak ada laporan penyewaan dengan atas nama <?=$pengguna?>
                <?php endif ?>
                
              </td>
            </tr>
          </tbody>
        <?php endif ?>
      </table>

      <div class="visually-hidden footer-label">
        <h5>
          Bekasi, <?=date('d-m-Y')?> <br>
          Dibuat Oleh
        </h5>
        <h5>(Admin)</h5>
      </div>

      <style>
        @media print {
          p.primary-header {
            font-size: 36px;
            margin-top: 0;
            margin-bottom: -10px;
            font-weight: bold;
          }
          p.secondary-header {
            font-size: 13.28px;
            font-weight: bold;
          }
          .table {
            font-size: 13px;
            width: 100%;
            border-collapse: collapse;
            color: #212529;
            vertical-align: middle;
            border-color: #dee2e6;
          }

          .table th, .table td {
            border: 1px solid #dee2e6;
            padding: .5rem .5rem;
          }
          .header-label {
            display: flex;
            justify-content: space-around;
            align-items: center;
            background: red;
            margin-bottom: 10px;
          }
          .footer-label {
            float: right;
            text-align: center;
          }
          @page {
              margin-top: 0;
              margin-bottom: 0;
          }
          body {
              padding-top: 72px;
              padding-bottom: 72px ;
          }
        }
      </style>
    </div>
  </div>
</div>


<script type="text/javascript" src="https://printjs-4de6.kxcdn.com/print.min.js"></script>

