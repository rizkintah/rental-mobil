<div class="card border border-white shadow-sm mt-3">
  <div class="card-header bg-white">
    <span data-feather="file" class="feather-16 me-1"></span> <?=$title?>
  </div>
  <div class="card-body">

    <div id="toolbar">
      <div class="row mt-3 mb-3">
        <div class="col-md-7 col-sm-12">
          <div class="row gy-2 gx-1 align-items-center">
            <div class="col-auto">
              <select name="tipe" class="form-select form-select-sm">
                <option selected>Cari Berdasarkan</option>
                <option value="penyewaan_id">No. Transaksi</option>
                <option value="nama_pengguna">Nama Penyewa</option>
                <option value="nama_jenis">Tipe Mobil</option>
                <option value="no_polisi">No. Polisi</option>
                <!-- <option value="status_sewa">Status</option> -->
              </select>
            </div>

            <div class="col-4">
              <input type="search" name="q" class="form-control form-control-sm" placeholder="Kata kunci">
            </div>

            <div class="col-auto">
              <button type="submit" class="btn btn-info btn-sm" id="btnCari">
                <span data-feather="search" class="feather-16"></span> Cari
              </button>
            </div>
          </div>
        </div>

        <div class="col-md-5 text-end">
          <div class="btn-group" role="group" aria-label="Basic radio toggle button group">
            <input type="radio" name="kat" value="" class="btn-check" name="btnradio" id="btn1" autocomplete="off" checked>
            <label class="btn btn-sm btn-outline-secondary" for="btn1">Semua Data</label>

            <input type="radio" name="kat" value="7" class="btn-check" name="btnradio" id="btn2" autocomplete="off">
            <label class="btn btn-sm btn-outline-secondary" for="btn2">
              Menunggu Verifikasi
            </label>

            <input type="radio" name="kat" value="6" class="btn-check" name="btnradio" id="btn3" autocomplete="off">
            <label class="btn btn-sm btn-outline-secondary" for="btn3">
              Mobil siap diambil
              <!-- <span class="badge bg-info text-dark">5</span> -->
            </label>

            <input type="radio" name="kat" value="2" class="btn-check" name="btnradio" id="btn4" autocomplete="off">
            <label class="btn btn-sm btn-outline-secondary" for="btn4">
              Selesai
              <!-- <span class="badge bg-info text-dark">5</span> -->
            </label>
          </div>
          
        </div>
      </div>
      
    </div>
    <table  data-toggle="table"
            id="tblPenyewaan"
            data-fixed-columns='true' 
            data-fixed-number= '2'
            --data-toolbar="#toolbar"
            data-detail-view="true"
            --data-mobile-responsive="true"
            data-detail-formatter="detailViewFormatter"
            data-pagination="true"
            data-filter-control="true"  
            --data-cell-style="cellStyle"
            data-only-info-pagination="false"
            data-query-params="qParams"
            data-response-handler="responseHandler"
            --data-show-refresh="true"
            data-side-pagination="server" 
            data-page-list="[10,30,40,50,60,100,200,300,1000]" 
            data-page-size="10"
            data-sortable="true"
            data-url="<?=site_url('admin/penyewaan/list')?>"
            class="table-striped table-hover bstable text-nowrap">
      <thead>
        <tr>
          <th data-field="penyewaan_id" 
              data-halign="center" 
              data-align="center" 
              data-valign="middle">No. Transaksi</th>

          <th data-field="nama_pengguna" 
              data-halign="center" 
              data-align="center" 
              data-valign="middle">Nama Penyewa</th>

          <th data-field="no_hp" 
              data-halign="center" 
              data-align="center" 
              data-valign="middle">No. HP Penyewa</th>

          <th data-field="no_polisi" 
              data-halign="center" 
              data-align="center" 
              data-valign="middle">No. Polisi</th>


          <th data-field="nama_jenis" 
              data-halign="center" 
              data-align="center" 
              data-valign="middle">Tipe Mobil</th>

          <th data-field="lama_sewa" 
              data-halign="center" 
              data-align="center" 
              data-valign="middle">Lama Sewa</th>

          <th data-field="total_harga_sewa" 
              data-halign="center" 
              data-align="center" 
              data-valign="middle"
              data-formatter="formatNumber">Total Harga</th>

          <th data-field="tanggal_sewa" 
              data-halign="center" 
              data-align="center" 
              data-valign="middle"
              data-formatter="tanggalFormatter">Tanggal Sewa</th>

          <th data-field="tanggal_kembali" 
              data-halign="center" 
              data-align="center" 
              data-valign="middle"
              data-formatter="tanggalFormatter">Tanggal Kembali</th>

          <th data-field="status_sewa" 
              data-halign="center" 
              data-align="center" 
              data-valign="middle"
              data-formatter="statusFormatter">Status Transaksi</th>
        </tr>
      </thead>
      
    </table>

  </div>
</div>