<div class="card border border-white shadow-sm mt-4">
  <div class="card-header bg-white">
    <strong><?=$title?></strong>
  </div>
  <div class="card-body">
    <form method="POST" action="<?=current_url()?>" enctype="multipart/form-data">
      <?php if ($this->uri->segment(4) == 'update'): ?>
        <input type="hidden" name="id" value="<?=encryptUrl($detail['jenis_id'])?>">
      <?php endif ?>
      <div class="row mb-3">
        <label for="imerk" class="col-sm-2 col-form-label">Merk Mobil</label>
        <div class="col-sm-10">
          <select name="merk" id="imerk" 
                  class="form-control <?=form_error('merk') ? 'is-invalid' : null?>">
            <option value="" disabled
                    <?=empty($detail['merk_id']) ? 'selected' : null;?>>Pilih Merk Mobil</option>
            <?php foreach ($merkMobil as $k => $v): ?>
              <option value="<?=$v['merk_id']?>" 
                      <?php echo set_select('merk', isset($detail['merk_id']) ? $detail['merk_id'] : $v['merk_id']); ?>>
                <?=$v['merk_nama']?>
              </option>
            <?php endforeach ?>
          </select>
          <?php echo form_error('merk','<div class="invalid-feedback">', '</div>'); ?>
        </div>
      </div>

      <div class="mb-3 row">
        <label for="ijenis" class="col-sm-2 col-form-label">Tipe Mobil</label>
        <div class="col-sm-10">
          <input type="text" 
                 name="jenis" 
                 class="form-control <?=form_error('jenis') ? 'is-invalid' : null?>" 
                 id="ijenis"
                 value="<?=set_value('jenis', isset($detail['nama_jenis']) ? $detail['nama_jenis'] : null)?>">
          <?php echo form_error('jenis','<div class="invalid-feedback">', '</div>'); ?>
        </div>
      </div>

      <hr>

      <div class="col-sm-10 offset-sm-2">
        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>">
        <a href="<?=site_url('admin/mobil/jenis')?>" class="btn btn-outline-danger">
          Kembali
        </a>
        <button type="submit" class="btn btn-dark ms-1">
          Simpan Data
        </button>
      </div>
    </form>
  </div>
</div>