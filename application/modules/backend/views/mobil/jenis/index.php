<div class="card border border-white shadow-sm mt-3">
  <div class="card-header bg-white">
    <span data-feather="layers" class="feather-16 me-1"></span> <?=$title?>
  </div>
  <div class="card-body">
    <div id="toolbar">
   
    </div>
    <table  data-toggle="table"
            data-toolbar="#toolbar"
            data-buttons="btnJenis"
            data-show-button-text="true"
            data-buttons-prefix="btn btn-sm btn-primary"
            class="table-striped table-hover">
      <thead>
        <tr>
          <th data-field="merk" data-halign="center" data-align="center" data-valign="middle">Tipe Mobil</th>
          <th data-field="icon" data-halign="center" data-align="center" data-valign="middle">Merk Icon</th>
          <th  data-halign="center" data-align="center" data-valign="middle" data-width="150">#</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($listJenis as $k => $v): ?>
          <tr>
            <td><?=$v['nama_jenis']?></td>
            <td><?=$v['merk_nama']?></td>
            <td>
              <a href="<?=site_url('admin/mobil/jenis/update/'.encryptUrl($v['jenis_id']))?>" 
                class="btn btn-sm btn-primary">
                <span data-feather="edit" class="feather-16"></span>
              </a>
              <a href="<?=site_url('admin/mobil/jenis/delete/'.encryptUrl($v['jenis_id']))?>" 
                 class="btn btn-sm btn-danger" 
                 onclick="return confirm('Are you sure want to delete?');">
                <span data-feather="trash" class="feather-16"></span>
              </a>
            </td>
          </tr>
        <?php endforeach ?>
      </tbody>
      
    </table>

  </div>
</div>