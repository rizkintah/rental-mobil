<?php  
  $merk_id  = isset($detail['merk_id']) ? $detail['merk_id'] : null;
  $jenis_id = isset($detail['jenis_id']) ? $detail['jenis_id'] : null;
?>

<div class="card border border-white shadow-sm mt-3">
  <div class="card-header bg-white">
    <strong><?=$title?></strong>
  </div>
  <div class="card-body">
    <form action="<?=current_url()?>" method="POST" enctype="multipart/form-data">
      <div class="row mb-3">
        <label for="imerekmobil" class="col-sm-2 col-form-label">Merk Mobil</label>
        <div class="col-sm-10">
          <select name="merk" id="imerekmobil" class="form-control <?=form_error('merk') ? 'is-invalid' : null?>">
            <option value="" disabled
                    <?=empty($_POST['merk']) ? 'selected' : null;?>>
                  Pilih Merk Mobil
            </option>
            
            <?php foreach ($merkMobil as $k => $v): ?>
              <option value="<?=$v['merk_id']?>" <?php echo set_select('merk', $merk_id, ($merk_id == $v['merk_id'])); ?>>
                <?=$v['merk_nama']?>
              </option>
            <?php endforeach ?>
          </select>
          <?php echo form_error('merk','<div class="invalid-feedback">', '</div>'); ?>
        </div>
      </div>

      <div class="row mb-3">
        <input type="hidden" name="curr_jenis" id="curr_jenis" value="<?=set_value('curr_jenis', $jenis_id )?>">
        <label for="ijenismobil" class="col-sm-2 col-form-label">Tipe Mobil</label>
        <div class="col-sm-10">
          <select name="jenis" id="ijenismobil" class="form-control <?=form_error('jenis') ? 'is-invalid' : null?>">
            <option value="" selected disabled>Pilih Tipe Mobil</option>
          </select>
          <?php echo form_error('jenis','<div class="invalid-feedback">', '</div>'); ?>
        </div>
      </div>

      <div class="row mb-3">
        <label for="iwarna" class="col-sm-2 col-form-label">Warna Mobil</label>
        <div class="col-sm-10">
          <input type="text" name="warna" id="iwarna" 
                 class="form-control <?=form_error('warna') ? 'is-invalid' : null?>"
                 value="<?=set_value('warna', isset($detail['warna']) ? $detail['warna'] : null)?>">
          <?php echo form_error('warna','<div class="invalid-feedback">', '</div>'); ?>
        </div>
      </div>

      <div class="row mb-3">
        <label for="inopol" class="col-sm-2 col-form-label">No. Polisi</label>
        <div class="col-sm-10">
          <input type="text" name="nopol" id="inopol" 
                 class="form-control <?=form_error('nopol') ? 'is-invalid' : null?>"
                 value="<?=set_value('nopol', isset($detail['no_polisi']) ? $detail['no_polisi'] : null)?>">
          <?php echo form_error('nopol','<div class="invalid-feedback">', '</div>'); ?>
        </div>
      </div>

      <div class="row mb-3">
        <label for="ihsewa" class="col-sm-2 col-form-label">Harga Sewa</label>
        <div class="col-sm-10">
          <input type="number" name="harga_sewa" id="ihsewa" 
                 class="form-control <?=form_error('harga_sewa') ? 'is-invalid' : null?>"
                 value="<?=set_value('harga_sewa', isset($detail['harga_sewa']) ? $detail['harga_sewa'] : null)?>">
          <?php echo form_error('harga_sewa','<div class="invalid-feedback">', '</div>'); ?>
        </div>
      </div>

      <div class="row mb-3">
        <label for="imgs" class="col-sm-2 col-form-label">Image Mobil</label>
        <div class="col-sm-10">
          <div class="d-grid gap-3">
            <button type="button" class="btn btn-primary" id="btnAddImage" type="button">Tambah Gambar Mobil</button>

            <div id="areaImg" class="row">
              <?php if (!empty($listImage)): ?>
                <?php foreach ($listImage as $k => $v): ?>
                  <div class='col-md-3 mb-2 area-img-<?=date('is')+$k?>'>
                    <div class="card" style="position:relative;">
                      <button type="button" class="btn btn-remove-image" onclick="removeImgCard('<?=date('is')+$k?>')">
                        <span class="fa fa-trash-alt"></span>
                      </button>

                      <img src="<?=base_url('_files/_mobil/'.$v['image_file'])?>" class="card-img-top" id="img-'<?=date('is')+$k?>'">
                      <div class="card-body">
                        <input type="hidden" name="imgMobilDelete[]" value="<?=$v['id']?>" readonly class="form-control form-control-sm mb-1">
                        <input type="text"  value="<?=$v['image_file']?>" disabled class="form-control form-control-sm mb-1">
                      </div>
                    </div>
                  </div>
                <?php endforeach ?>
              <?php endif ?>
            </div>
          </div>
        </div>
      </div>
      
      <hr>

      <div class="col-sm-10 offset-sm-2">
        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>">
        <a href="<?=site_url('admin/mobil')?>" class="btn btn-outline-danger">
          Kembali
        </a>
        <button type="submit" class="btn btn-dark ms-1">
          Simpan Data
        </button>
      </div>
    </form>
  </div>
</div>