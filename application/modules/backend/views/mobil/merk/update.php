<div class="card border border-white shadow-sm mt-4">
  <div class="card-header bg-white">
    <strong><?=$title?></strong>
  </div>
  <div class="card-body">
   
    <form method="POST" action="<?=current_url()?>" enctype="multipart/form-data">
      <input type="hidden" name="id" class="form-control" value="<?=$this->uri->segment(5)?>">
      <div class="mb-3 row">
        <label for="imerk" class="col-sm-2 col-form-label">Merk Mobil</label>
        <div class="col-sm-10">
          <input type="text" 
                 name="merk" 
                 class="form-control <?=form_error('merk') ? 'is-invalid' : null?>" 
                 id="imerk"
                 value="<?=set_value('merk', $detail['merk_nama'])?>">
          <?php echo form_error('merk','<div class="invalid-feedback">', '</div>'); ?>
        </div>
      </div>

      <div class="mb-3 row">
        <label for="ifile" class="col-sm-2 col-form-label">Merk Icon</label>
        <div class="col-sm-10">
          <div class="mb-3">
            <div class="input-group">
              <input class="form-control <?=form_error('merk_icon') ? 'is-invalid' : null?>" 
                     name="merk_icon" 
                     type="file" 
                     value="<?=set_value('merk_icon', $detail['merk_icon'])?>"
                     id="ifile">
              <button class="btn btn-secondary" type="button" data-bs-toggle="collapse" href="#img-existing">Show/hide Existing File</button>
              <?php echo form_error('merk_icon','<div class="invalid-feedback">', '</div>'); ?>

            </div>
          </div>

          <div class="collapse" id="img-existing">
            <div class="card">
              <div class="card-header">
                filename : <?=$detail['merk_icon']?>
              </div>
              <div class="card-body">
                <img src="<?=base_url('_files/_merk/'.$detail['merk_icon'])?>" class="img-fluid img-rounded">
              </div>
            </div>
          </div>
        </div>
      </div>

      <hr>

      <div class="col-sm-10 offset-sm-2">
        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>">
        <a href="<?=site_url('admin/mobil/merk')?>" class="btn btn-outline-danger">
          Kembali
        </a>
        <button type="submit" class="btn btn-dark ms-1">
          Simpan Data
        </button>
      </div>
    </form>
  </div>
</div>