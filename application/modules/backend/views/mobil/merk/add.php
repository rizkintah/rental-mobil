<div class="card border border-white shadow-sm mt-4">
  <div class="card-header bg-white">
    <strong><?=$title?></strong>
  </div>
  <div class="card-body">
   
    <form method="POST" action="<?=current_url()?>" enctype="multipart/form-data">

      <div class="mb-3 row">
        <label for="imerk" class="col-sm-2 col-form-label">Merk Mobil</label>
        <div class="col-sm-10">
          <input type="text" 
                 name="merk" 
                 class="form-control <?=form_error('merk') ? 'is-invalid' : null?>" 
                 id="imerk"
                 value="<?=set_value('merk')?>">
          <?php echo form_error('merk','<div class="invalid-feedback">', '</div>'); ?>
        </div>
      </div>

      <div class="mb-3 row">
        <label for="ifile" class="col-sm-2 col-form-label">Merk Icon</label>
        <div class="col-sm-10">
          <div class="mb-3">
            <input class="form-control <?=form_error('merk_icon') ? 'is-invalid' : null?>" 
                   name="merk_icon" 
                   type="file" 
                   value="<?=set_value('merk_icon')?>"
                   id="ifile">
            <?php echo form_error('merk_icon','<div class="invalid-feedback">', '</div>'); ?>
          </div>
        </div>
      </div>

      <hr>

      <div class="col-sm-10 offset-sm-2">
        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>">
        <a href="<?=site_url('admin/mobil/merk')?>" class="btn btn-outline-danger">
          Kembali
        </a>
        <button type="submit" class="btn btn-dark ms-1">
          Simpan Data
        </button>
      </div>
    </form>
  </div>
</div>