<div class="card border border-white shadow-sm mt-3">
  <div class="card-header bg-white">
    <span data-feather="layers" class="feather-16 me-1"></span> <?=$title?>
  </div>
  <div class="card-body">
    <div id="toolbar">
      <div class="row gy-2 gx-3 align-items-center">
        <div class="col-auto">
          <select name="tipe" class="form-select form-select-sm">
            <option selected>Cari Berdasarkan</option>
            <option value="nama_jenis">Jenis Mobil</option>
            <option value="merk_nama">Merk Mobil</option>
            <option value="no_polisi">No. Polisi</option>
            <option value="status">Status</option>
          </select>
        </div>

        <div class="col-auto">
          <input type="search" name="q" class="form-control form-control-sm" placeholder="Kata kunci">
        </div>

        <div class="col-auto">
          <button type="submit" class="btn btn-info btn-sm" id="btnCariMobil">
            <span data-feather="search" class="feather-16"></span> Cari
          </button>
        </div>
      </div>
    </div>
    <table  data-toggle="table"
            id="tblMobil"
            data-buttons="btnMobil"
            data-show-button-text="true"
            data-buttons-prefix="btn btn-sm btn-primary"
            data-toolbar="#toolbar"
            data-pagination="true"
            data-filter-control="true"  
            --data-cell-style="cellStyle"
            data-only-info-pagination="false"
            data-query-params="qParams"
            data-response-handler="responseHandler"
            --data-show-refresh="true"
            data-side-pagination="server" 
            data-page-list="[10,30,40,50,60,100,200,300,1000]" 
            data-page-size="10"
            data-sortable="true"
            data-url="<?=site_url('admin/mobil/lists')?>"
            class="table-striped table-hover bstable text-nowrap">
      <thead>
        <tr>
          <th data-field="nama_jenis" data-halign="center" data-align="center" data-valign="middle">Tipe Mobil</th>
          <th data-field="merk_nama" data-halign="center" data-align="center" data-valign="middle">Merk</th>
          <th data-field="warna" data-halign="center" data-align="center" data-valign="middle">Warna</th>
          <th data-field="no_polisi" data-halign="center" data-align="center" data-valign="middle">No. Polisi</th>
          <th data-field="harga_sewa" data-halign="center" data-align="center" 
              data-valign="middle" data-formatter="formatNumber">Harga Sewa</th>

          <th data-field="status" data-halign="center" data-align="center" data-valign="middle">Status</th>
          <th data-field="mobil_id" data-halign="center" 
              data-align="center" data-valign="middle" 
              data-width="100" data-formatter="btnActionMobil">#</th>
        </tr>
      </thead>
      
    </table>

  </div>
</div>