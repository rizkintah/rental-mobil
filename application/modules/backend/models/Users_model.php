<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {

	private $vUsers 			= 'v_pengguna';
	private $tUsers 			= 'pengguna';
	private $tUsersGroup 	= 'group_pengguna';
	

	function getListUsers($filter, $offset, $limit, $sort, $order, $search) {
		$wFilter = null;

    # filter => ini blm dipakai krn tdk menggunakan filter control
      if (!empty($filter)) {
        $i=1;
        foreach ($filter as $fieldFilter => $value) {
          if ($i==1) {
            $wFilter .= " UPPER(".$fieldFilter.") LIKE UPPER('%".$value."%')";
          }else{
            $wFilter .= " AND UPPER(".$fieldFilter.") LIKE UPPER('%".$value."%')";
          }
          $i++;
        }
        $wFilter = "AND (\n$wFilter)";
      }
    # filter

    # query
      $query = "SELECT A.*
                FROM {$this->vUsers} A
                WHERE 1=1
                {$search}";
    # end query
    $qData = $this->bs_table->create_query($query, $limit, $offset, $sort, $order);

    foreach($qData as $k => $v) {
      $qData[$k]['pengguna_id'] = encryptUrl($v['pengguna_id']);
    }


    $qJml  = $this->bs_table->create_query_count($query);
    return array('total' => $qJml, 'rows' => $qData);
	}

  function getListGroup() {
    $this->db->select('*');
    $this->db->where('group_id !=', 2);
    return $this->db->get($this->tUsersGroup)->result_array();
  }

  function getDetailUserBy($userID) {
    $this->db->where('pengguna_id', $userID);
    return $this->db->get($this->tUsers)->row_array();
  }
 
  function insertUser($data) {
    return $this->db->insert($this->tUsers, $data);
  }

  function updateUser($userID, $data) {
    $this->db->where('pengguna_id', $userID);
    return $this->db->update($this->tUsers, $data);
  }

  function deleteUser($userID) {
    $this->db->where('pengguna_id', $userID);
    return $this->db->delete($this->tUsers);
  }

  function checkUsernameExisting($str, $id = null) {
      $this->db->where('LOWER(username)', strtolower($str));
      if($id)  $this->db->where_not_in('pengguna_id', $id);

      $q = $this->db->get($this->tUsers)->num_rows();
      if ($q == 0) {
        return true;
      }else{
        return false;
      }
    }

}

/* End of file Users_model.php */
/* Location: ./application/modules/backend/models/Users_model.php */