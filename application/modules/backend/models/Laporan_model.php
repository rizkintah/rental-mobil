<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_model extends CI_Model {

	private $vMobil 		= 'v_mobil';
	private $tPenyewaan = 'penyewaan';
	private $vUsers 		= 'v_pengguna';
	
	function getLaporanPenyewaan($starDate, $endDate, $filter, $namaPengguna)
	{
		$this->db->select('p.tanggal_sewa, p.penyewaan_id, vp.nama_pengguna, 
											 vm.merk_nama, vm.nama_jenis, vm.no_polisi, p.total_harga_sewa');

		$this->db->from("{$this->tPenyewaan} p");
		$this->db->join("{$this->vMobil} vm", "p.mobil_id = vm.mobil_id");
		$this->db->join("{$this->vUsers} vp", "p.pengguna_id  = vp.pengguna_id");
		$this->db->where('p.status_sewa', 2);

		if ($filter == 'daterange') {
			$this->db->where("DATE_FORMAT(tanggal_sewa,'%Y-%m-%d') BETWEEN '{$starDate}' AND '{$endDate}'");
		}elseif($filter == "pengguna") {
			$this->db->where('nama_pengguna', $namaPengguna);
		}

		return $this->db->get()->result_array();
	}

	function getListPenyewaBy($keyword) {
		$this->db->select('pengguna_id, nama_pengguna');
		$this->db->like('nama_pengguna', $keyword, 'BOTH');
		$this->db->where('nama_group', 'user');
		$data = $this->db->get($this->vUsers);
		$arrData = array();

		foreach($data->result_array() as $v) {
			$arrData[] = array("value"=>$v['nama_pengguna'],"label"=>$v['nama_pengguna']);
		}

		return $arrData;
	}

}

/* End of file Laporan_model.php */
/* Location: ./application/modules/backend/models/Laporan_model.php */