<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penyewaan_model extends CI_Model {

	private $tMerkMobil = 'merk_mobil';
	private $tMobil 		= 'mobil';
	private $tMobilImg 	= 'mobil_images';
	private $vMobil 		= 'v_mobil';
	private $tPenyewaan = 'penyewaan';
	private $tPengguna 	= 'pengguna';

	function get_list_penyewaan($filter, $offset, $limit, $sort, $order, $search, $statusSewa) {
		$wStatusSewa = null;
		if(!empty($statusSewa)) $wStatusSewa = "AND status_sewa='{$statusSewa}'";

		# sql
			$sql = "SELECT p.*, pg.alamat, pg.email, pg.nama_pengguna, pg.no_hp, pg.no_identitas, pg.file_ktp,
										 vm.harga_sewa, vm.jenis_id,  vm.nama_jenis, vm.merk_id, vm.merk_nama, vm.no_polisi
							FROM {$this->tPenyewaan} p, {$this->tPengguna} pg, {$this->vMobil} vm 
							WHERE p.pengguna_id = pg.pengguna_id
							AND p.mobil_id = vm.mobil_id
							{$wStatusSewa}
							{$search}
							ORDER BY p.tanggal_transaksi DESC";
		# end sql

		# filter => ini blm dipakai krn tdk menggunakan filter control
      if (!empty($filter)) {
        $i=1;
        foreach ($filter as $fieldFilter => $value) {
          if ($i==1) {
            $wFilter .= " UPPER(".$fieldFilter.") LIKE UPPER('%".$value."%')";
          }else{
            $wFilter .= " AND UPPER(".$fieldFilter.") LIKE UPPER('%".$value."%')";
          }
          $i++;
        }
        $wFilter = "AND (\n$wFilter)";
      }
    # filter

    $qData = $this->bs_table->create_query($sql, $limit, $offset, $sort, $order);
    $qJml  = $this->bs_table->create_query_count($sql);

    return array('total' => $qJml, 'rows' => $qData);
	}

	function updateStatusPenyewaan($penyewaanID, $data, $options) {
		$data['admin_id'] = $this->session->userdata('userID');

		$this->db->where('penyewaan_id', $penyewaanID);
		$this->db->update($this->tPenyewaan, $data);


		if (in_array($options['jenisTransaksi'], array('batal', 'selesai'))) {
			$this->db->where('mobil_id', $options['mid']);
			$this->db->update($this->tMobil, array('status' => 'tersedia'));
		}

		return true;
	}

}

/* End of file Penyewaan_model.php */
/* Location: ./application/modules/backend/models/Penyewaan_model.php */