<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_mobil extends CI_Model {

	private $tJenisMobil = 'jenis_mobil';
	private $vJenisMobil = 'v_jenis_mobil';


	function getLists() {
		return $this->db->get($this->vJenisMobil)->result_array();
	}

	function getDetailByID($id) {
		$this->db->where('jenis_id', $id);
		return $this->db->get($this->tJenisMobil)->row_array();
	}	

	function checkJenisDuplicate($str, $id = null) {
		$this->db->where('LOWER(nama_jenis)', strtolower($str));

		if (!empty($id)) $this->db->where_not_in('jenis_id', $id);

		$qData = $this->db->get($this->tJenisMobil)->num_rows();
		if ($qData == 0) {
			return true;
		}else{
			return false;
		}
	}	

	function insertToDB($data)
	{
		return $this->db->insert($this->tJenisMobil, $data);
	}

	function updateToDB($id, $data) {
		$this->db->where('jenis_id', $id);
		return $this->db->update($this->tJenisMobil, $data);
	}

	function deleteFromDB($id)
	{
		$this->db->where('jenis_id', $id);
		return $this->db->delete($this->tJenisMobil);
	}

	function getListListByMerkID($merkID) {
		$this->db->where('merk_id', $merkID);
		return $this->db->get($this->tJenisMobil)->result_array();
	}

}

/* End of file Jenis_mobil.php */
/* Location: ./application/modules/backend/models/Jenis_mobil.php */