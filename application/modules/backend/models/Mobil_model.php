<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mobil_model extends CI_Model {
	
	private $tMerkMobil = 'merk_mobil';
	private $tMobil 		= 'mobil';
	private $tMobilImg 	= 'mobil_images';
	private $vMobil 		= 'v_mobil';

	# mobil
		function getListMobil($filter, $offset, $limit, $sort, $order, $search) {
			$wFilter = null;

	    # filter => ini blm dipakai krn tdk menggunakan filter control
	      if (!empty($filter)) {
	        $i=1;
	        foreach ($filter as $fieldFilter => $value) {
	          if ($i==1) {
	            $wFilter .= " UPPER(".$fieldFilter.") LIKE UPPER('%".$value."%')";
	          }else{
	            $wFilter .= " AND UPPER(".$fieldFilter.") LIKE UPPER('%".$value."%')";
	          }
	          $i++;
	        }
	        $wFilter = "AND (\n$wFilter)";
	      }
	    # filter

	    # query
	      $query = "SELECT A.*
	                FROM {$this->vMobil} A
	                WHERE status <> 'deleted'
	                {$search}";
	    # end query
	    $qData = $this->bs_table->create_query($query, $limit, $offset, $sort, $order);

	    foreach($qData as $k => $v) {
	      $qData[$k]['mobil_id'] = encryptUrl($v['mobil_id']);
	    }
	    $qJml  = $this->bs_table->create_query_count($query);
	    return array('total' => $qJml, 'rows' => $qData);
		}

		function insertMobilToDB($data) {
			$query = $this->db->insert($this->tMobil, $data);
			$insert_id = $this->db->insert_id();
			return $insert_id;
		}

		function updateMobilToDB($id, $data) {
			$this->db->where('mobil_id', $id);
			$query = $this->db->update($this->tMobil, $data);
			return $query;
		}

		function insertImgMobil($mobilID, $arrImg) {
			foreach($arrImg as $v) {
				$arr = array(
					'mobil_id' 	=> $mobilID,
					'image_file'=> $v
				);
				$this->db->insert($this->tMobilImg, $arr);
			}

			return true;
		}

		function deleteMobilImageIn($arrID, $mobilID) {
			$this->db->where('mobil_id', $mobilID);
			$this->db->where_not_in('id', $arrID);
			return $this->db->delete($this->tMobilImg);
		}

		function getDetailMobilByID($mobilID) {
			$this->db->where('mobil_id', $mobilID);
			$q = $this->db->get($this->vMobil)->row_array();
			return $q;
		}

		function getMobilImages($mobilID) {
			$this->db->where('mobil_id', $mobilID);
			return $this->db->get($this->tMobilImg)->result_array();
		}

		function deleteMobilFromDB($mobilID)
		{
			$this->db->where('mobil_id', $mobilID);
			$del = $this->db->update($this->tMobil, array('status' => 'deleted'));

			return true;
		}
	# end mobil

	# merk mobil
		function getListMerkMobil() {
			return $this->db->get($this->tMerkMobil)->result_array();
		}

		function getDetailMerkMobilByID($id) {
			$this->db->where('merk_id', $id);
			return $this->db->get($this->tMerkMobil)->row_array();
		}

		function insertMerkMobil($data) {
			return $this->db->insert($this->tMerkMobil, $data);
		}

		function updateMerkMobil($id, $data) {
			$this->db->where('merk_id', $id);
			return $this->db->update($this->tMerkMobil, $data);
		}

		function deleteMerkMobilByID($id)
		{
			$this->db->where('merk_id', $id);
			return $this->db->delete($this->tMerkMobil);
		}

		function checkMerkMobilExisting($str, $id = null) {
			$this->db->where('LOWER(merk_nama)', strtolower($str));
			if($id)  $this->db->where_not_in('merk_id', $id);

			$q = $this->db->get($this->tMerkMobil)->num_rows();
			if ($q == 0) {
				return true;
			}else{
				return false;
			}
		}
	# end merk mobil
	

}

/* End of file Mobil_model.php */
/* Location: ./application/modules/backend/models/Mobil_model.php */