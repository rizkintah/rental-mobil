<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!is_logged()) redirect(site_url());
		$this->load->model('Dashboard_model', 'D_model');
		$this->rzkt->updateStatusCancelByBatasKonfirmasi();
	}


	function get_list_user_sewa() {
		$status = $this->input->get('s') ? getStatusName($this->input->get('s')) : null;
		$data['title'] = null;
		$data['list']  = $this->D_model->getListPenyewaanBy($status); 
		theme_frontend('user/list_penyewaan',$data);
	}

	function get_data_konfirmasi() {
		if (!empty($_FILES['bukti_transfer']['name'])) {
			$penyewaanID = $this->input->post('pid');

			$uploadImg = upload_single_file('./_files/_bukti_pembayaran/','bukti_transfer', '*');
			$imgFile = $uploadImg['file_name'];

			$update = $this->D_model->updateKonfirmasiPembayaran($penyewaanID, $imgFile);
			
			if ($update) {
				$this->session->set_flashdata('notif', 'Bukti pembayaran berhasil di upload');
			}else{
				$this->session->set_flashdata('notif', 'Bukti pembayaran gagal di upload');
			}
			redirect('user/penyewaan');
		}else if ($this->input->is_ajax_request()) {
			$penyewaanID = $this->input->get('pid');
			$data['detail'] = $this->D_model->getDetailPenyewaanBy($penyewaanID);
			$this->load->view('user/konfirmasi', $data);
		}else{
			echo "no allowed access";
		}
	}

	function get_invoice($noInvoice) {
		$data['noInvoice'] = $noInvoice;
		$data['detail'] = $this->D_model->getDetailInvoice($noInvoice);
		$this->load->view('user/invoice', $data);
 	}

}

/* End of file Dashboard.php */
/* Location: ./application/modules/frontend/controllers/Dashboard.php */