<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('backend/Mobil_model', 'M_mobil');
		$this->load->model('backend/Jenis_mobil', 'M_jenis');
		$this->load->model('Frontend_model', 'F_model');
		$this->rzkt->updateStatusCancelByBatasKonfirmasi();
		// $this->config->load('apps_setting', TRUE);

	}

	public function index()
	{
		$data['title'] = null;
		$data['merkMobil'] = $this->M_mobil->getListMerkMobil();
		theme_frontend('index',$data);
	}

	function get_list_mobil() {
		$merkID  = decryptUrl($this->input->get('mid'));
		$jenisID = decryptUrl($this->input->get('jid'));
		$tglSewa = implode('-', array_reverse(explode('/', $this->input->get('tsewa'))));
		$JmlHari = $this->input->get('hari');

		$data['lists'] 	= $this->F_model->getListMobil($merkID, $jenisID, $tglSewa, $JmlHari);
		$data['tglSewa'] = nice_date($tglSewa, 'Ymd');
		$data['JmlHari'] = $JmlHari;
		$this->load->view('list_mobil', $data);
	}

	function get_sewa_mobil($mobilID) {
		$data['title'] = "Sewa Mobil";
		$data['detail'] = $this->M_mobil->getDetailMobilByID(decryptUrl($mobilID));
		$data['listImage'] = $this->F_model->getListMobilImage(decryptUrl($mobilID));
		$data['hari'] = $this->input->get('hari');
		$data['tsewa'] = $this->input->get('tsewa');
		
		if (!is_logged()) {
			redirect('login?mid='.$mobilID.'&hari='.$data['hari'].'&tsewa='.$data['tsewa']);
		}else{
			if (!empty($_POST)) {
				$insert = $this->_insert_sewa_mobil($_POST);
				$this->session->set_flashdata('notif', 'Silahkan lakukan pembayaran dengan cara transfer dan konfirmasi.');
				redirect('user/penyewaan');
			}else{
				theme_frontend('sewa',$data);
			}
		}
	}

	function _insert_sewa_mobil($data) {
		$arrData = array(
			'penyewaan_id' 			=> $this->rzkt->getTransaksiID(),
			'pengguna_id'	 			=> $this->session->userdata('userID'),
			'mobil_id'	 	 			=> decryptUrl($data['mid']),
			'lama_sewa'	 	 			=> $data['hari'],
			'total_harga_sewa'	=> $data['total_harga'],
			'tanggal_sewa'			=> date('Y-m-d', $data['sd']),
			'tanggal_kembali'		=> date('Y-m-d', $data['ed']),
			'tanggal_transaksi'	=> date('Y-m-d H:i:s'),
			'batas_konfirmasi'	=> date('Y-m-d H:i:s', strtotime('1 days'))
		);

		$ins = $this->F_model->insertPenyewaan($arrData);
	}

	function get_list_jenis_by_merk() {
		if ($this->input->is_ajax_request()) {
			$id = decryptUrl($this->input->get('mid'));
			$data['curr_jenis'] 	= $this->input->get('current') ? decryptUrl($this->input->get('current')) : null;
			$data['lists'] = $this->M_jenis->getListListByMerkID($id);
			$this->load->view('list_jenis', $data);
		}else{
			echo "no allowed access";
		}
	}

}

/* End of file Frontend.php */
/* Location: ./application/modules/frontend/controllers/Frontend.php */