<?php  
	$hari 			= $this->input->get('hari') ? $this->input->get('hari') : 1; 
	$tsewa 			= $this->input->get('tsewa') ? nice_date($this->input->get('tsewa'), 'd/m/Y') : date('d/m/Y'); 
	$start 			= date('d F Y', strtotime($this->input->get('tsewa')));
	$end 				= date('d F Y', strtotime($this->input->get('tsewa')."+{$hari}day"));
	$hargaSewa 	= $hari * $detail['harga_sewa'];
?>

<div class="row mt-5">
	<div class="col-md-8">
		<div class="nama-mobil mb-4">
			<h4><?=$detail['merk_nama']?> <?=$detail['nama_jenis']?></h4>
		</div>
	  <div id="carMobil" class="carousel carousel-dark slide" data-bs-ride="carousel">
		  <div class="carousel-inner">
		  	<?php foreach ($listImage as $k => $v): ?>
		    	<div class="carousel-item <?=$k == 0 ? 'active' : null?>"> 
		    		<img src="<?=base_url('_files/_mobil/'.$v['image_file'])?>" alt="<?=$v['image_file']?>" width="100%"> 
		    	</div>
		  	<?php endforeach ?>
		  </div>
	  	<ul class="carousel-indicators list-inline">
	  		<?php foreach ($listImage as $k => $v): ?>
	  			<li class="list-inline-item">
		  			<a data-bs-target="#carMobil" 
		  				 data-bs-slide-to="<?=$k?>" 
		  				 <?=$k==0 ? 'class="active"' : null;?> 
		  				 aria-current="true" 
		  				 aria-label="Slide <?=$k+1?>">
		  				 <img src="<?=base_url('_files/_mobil/'.$v['image_file'])?>" class="img-fluid">
		  			</a>
		  		</li>
	  		<?php endforeach ?>
	  	</ul>
		</div>
	</div>

	<div class="col-md-4">
		<div class="area-data-diri mt-5">
			<h5>Detail Penyewaan</h5>
			<div class="divider"></div>
			<form action="<?=current_url()?>" method="POST" class="mt-4">
				<input type="hidden" name="mid" value="<?=$this->uri->segment(2)?>">
				<input type="hidden" name="sd" value="<?=strtotime($start)?>">
				<input type="hidden" name="ed" value="<?=strtotime($end)?>">
				<input type="hidden" name="total_harga" value="<?=$hargaSewa?>">
				<input type="hidden" name="harga_sewa" value="<?=$detail['harga_sewa']?>">
				<div class="mb-3">
				  <label for="tgl_sewa" class="form-label mb-1">Tanggal Sewa</label>
				  <div class="input-group date" id="dtsewa" data-target-input="nearest">
					  <input type="text" name="tanggal" disabled class="form-control datetimepicker-input dtsewa" data-target="#dtsewa" placeholder="dd/mm/yyyy" data-toggle="datetimepicker" value="<?=$tsewa?>">
					  <button class="btn btn-secondary" type="button" data-target="#dtsewa" data-toggle="datetimepicker">
					  	<i class="fas fa-calendar-alt"></i>
					  </button>
					</div>
				</div>

				<div class="mb-4">
				  <label for="tgl_sewa" class="form-label mb-1">Lama Sewa</label>
				  <input name="hari" type="number" value="<?=$hari?>" min="1" max="20" step="1" class="spinner-input-sewa" />
				</div>

				<div class="mb-3">
				  <label for="tgl_sewa" class="form-label mb-1">Harga Sewa</label>
				  <div class="row">
				  	<div class="col-sm-8 text-start" id="ketSewa">
				  		<small>
				  			<strong>
									<?=date('d M Y', strtotime($start))?> - <?=date('d M Y', strtotime($end))?>
				  			</strong>
				  		</small>
				  	</div>
				  	<div class="col-sm-4 text-end" id="hargaSewa">
				  			<small><strong><?=number_format($hargaSewa)?></strong></small>
				  	</div>
				  </div>
				</div>

				<div class="divider"></div>
				<div class="row mt-3">
					<div class="col-sm-5 text-start">
						<h5>Total Harga</h5>
					</div>
					<div class="col-sm-7 text-end" id="totalHarga">
						<h5>Rp. <?=number_format($hargaSewa)?></h5>
					</div>
				</div>

				<div class="mt-2">
					<div class="d-grid gap-2">
						<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>">
						<button class="btn btn-warning btn-sm" type="submit">Sewa Sekarang</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>


<style type="text/css">
	.nama-mobil h4 {
		font-weight: bold;
		position: relative;
	}

	.nama-mobil h4:after {
		background-color: #FFC007;
		content: "";
		position: absolute;
		width: 5vw;
		height: 3px;
		left: 0;
		bottom: -8px;
	}

	.divider {
		width: 100%;
		height: 2px;
		background-color: #191919;
	}
</style>