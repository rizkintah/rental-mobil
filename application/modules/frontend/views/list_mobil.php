<?php foreach ($lists as $k => $v): ?>
	<div class="col-sm-3 mb-3">
		<div class="card overflow-hidden">
		  <img src="<?=base_url('_files/_mobil/'.$v['image_file'])?>" class="card-img-top" alt="<?=$v['image_file']?>"> 
		  <div class="card-body">
		    <h6 class="card-title"><?=$v['merk_nama']?> <?=$v['nama_jenis']?></h6>
		    <div class="card-text text-center card-text-list">
				  <!-- <i class="fas fa-tag"></i>  -->
				  Harga: <strong><?=number_format($v['harga_sewa'])?> / Hari</strong>
				</div>

				<div class="d-grid gap-2">
		   		<a href="<?=site_url('sewa/'.encryptUrl($v['mobil_id'])).'?hari='.$JmlHari.'&tsewa='.$tglSewa?>" class="btn btn-warning btn-sm">Sewa</a>
				</div>
		  </div>
		</div>
	</div>
<?php endforeach ?>

