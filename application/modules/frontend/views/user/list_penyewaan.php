<?php  
  $s = $this->input->get('s');
  $disabled = $s ? 'disabled' : null;
?>

<div class="alert alert-secondary mt-5">
  <i class="fa fa-align-justify me-1"></i> <strong>Daftar Penyewaan</strong>
</div>

<div class="mt-2 mb-4 d-flex justify-content-between align-items-center">
  <ul class="nav nav-pills nav-dashboard">
    <li class="nav-item disabled">
      <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Status</a>
    </li>
    <li class="nav-item">
      <a class="nav-link <?=empty($s) ? 'active' : null;?>" aria-current="page" href="<?=current_url()?>">Berlangsung</a>
    </li>
    <li class="nav-item">
      <a class="nav-link <?=$s == 'Selesai Sewa' ? 'active' : null;?>" href="?s=Selesai Sewa">Selesai</a>
    </li>
    <li class="nav-item">
      <a class="nav-link <?=$s == 'Batal' ? 'active' : null;?>" href="?s=Batal">Batal</a>
    </li>
  </ul>

  <button type="button" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#modalCaraBayar">
    Cara Pembayaran
  </button>
  
</div>

<?php if (count($list) > 0): ?>
  <?php foreach ($list as $k => $v): ?>
    <div class="card mb-3 border-0">
      <div class="card-body shadow-sm">
        <div class="d-flex justify-content-between align-items-center mb-2">
          <h6 class="card-subtitle text-muted fs-7">
            <span class="me-1"><strong>No. Transaksi: <?=$v['penyewaan_id']?> |</strong></span>
            <i class="fas fa-calendar-alt me-1"></i> <?=date('d M Y', strtotime($v['tanggal_transaksi']))?>
            <span class="badge bg-info text-dark rounded-pill ms-2"><?=getStatusName($v['status_sewa'])?></span>
          </h6>
          <a href="<?=site_url('invoice/'.$v['penyewaan_id'])?>" target="_blank" 
            class="badge rounded-pill bg-success text-decoration-none px-3">
            Invoice
          </a>
        </div>
        
        <div class="card-body">
          <div class="row g-3 align-items-center">
            <div class="col-md-2">
              <img src="<?=base_url('_files/_mobil/'.$v['image_file'])?>" class="img-fluid rounded">
            </div>
            
            <div class="col-md-6">
              <h6 class="nama-mobil"><?=$v['merk_nama']?> <?=$v['nama_jenis']?></h6>
              
              <div class="fw-lighter">
                <small>Lama Sewa : <?=$v['lama_sewa']?> Hari</small>
                <small>
                  (<?=date('d M Y', strtotime($v['tanggal_sewa']))?> 
                    - 
                    <?=date('d M Y', strtotime($v['tanggal_kembali']))?>)
                </small>
              </div>

              <div class="fw-lighter">
                <small>Total Bayar : <?=$v['lama_sewa']?> Hari x <?=number_format($v['harga_sewa'])?></small>
                <small class="fw-bold">
                 | Rp.  <?=number_format($v['total_harga_sewa'])?>
                </small>
              </div>

              <?php if (getStatusName($v['status_sewa']) == 'Menunggu Pembayaran'): ?>
                <div class="fw-bold text-success">
                  <small>
                    Lakukan pembayaran sebelum: <?=date('d M Y H:i', strtotime($v['batas_konfirmasi']))?>
                  </small>
                </div>
              <?php endif ?>
              
            </div>

            <div class="col-md-4 text-end">
              <button type="button" class="btn btn-danger btn-sm btn-konfirmasi" <?=$v['status_sewa'] <> '4' ? 'disabled' : ''?> data-pid="<?=$v['penyewaan_id']?>">
                Konfirmasi Pembayaran
              </button>
            </div>

          </div>
        </div>
      </div>
    </div>
  <?php endforeach ?>
<?php else: ?>
 <div class="alert alert-warning text-center">
    <strong>Data tidak ditemukan</strong>
  </div>
<?php endif ?>




<div class="modal fade" id="modalCaraBayar" tabindex="-1" aria-labelledby="modalCaraBayar" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalCaraBayar">Cara Pembayaran</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <p>
          Berikut panduan melakukan pembayaran:
        </p>
        <ol>
          <li>
            Setelah anda memilih mobil dan mengisi detail penyewaan, maka anda akan diarahkan menuju halaman penyewaan.
          </li>
          <li>
            Lakukan pembayaran sesuai dengan jumlah tagihan yang tertera dan transfer ke akun bank berikut: 
            <table class="table table-sm table-striped mt-2">
              <tr>
                <td>No. Rekening</td>
                <td><strong>0060002148421</strong></td>
              </tr>
              <tr>
                <td>Nama Bank</td>
                <td>Bank Mandiri</td>
              </tr>
              <tr>
                <td>Atas Nama</td>
                <td>Trikarya Sejati</td>
              </tr>
            </table>
          </li>
          <li>
            Pastikan pembayaran kamu sudah berhasil dan <strong>unggah bukti</strong> transfer kamu dengan cara klik tombol <strong>Konfirmasi Pembayaran</strong> lalu unggah bukti transfer. Agar admin dapat melakukan proses verifikasi pembayaran.
          </li>
          
        </ol>
      </div>
    </div>
  </div>
</div>

<style type="text/css">
  ol {
    padding: 0; 
  }
  ol li {
    margin-bottom: 10px;
    counter-increment: a;
    line-height: 1.5;
    padding-left: 36px;
    position: relative;
    list-style-type: none;
    font-size: 1rem;
  }
  ol li:before {
    content: counter(a);
    margin-right: 20px;
    font-size: 0.8571428571428571rem;
    background-color: var(--N150,#B5BBC5);
    color: var(--N0,#FFFFFF);
    font-weight: 400;
    border-radius: 100%;
    position: absolute;
    left: 0;
    top: 2px;
    width: 24px;
    height: 24px;
    text-align: center;
    font-weight: bold;
    line-height: 25px;
  }

</style>