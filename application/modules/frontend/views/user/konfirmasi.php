<ul class="list-group list-group fs-6">
  <li class="list-group-item">
  	<small class="fw-bold"><?=$detail['merk_nama']?> <?=$detail['nama_jenis']?></small>
  </li>
  <li class="list-group-item">
  	<div class="d-flex w-100 justify-content-between">
      <small class="fw-bold">Lama Sewa</small>
    </div>
    <small>
  		<?=$detail['lama_sewa']?> Hari
    	(<?=date('d M Y', strtotime($detail['tanggal_sewa']))?> - <?=date('d M Y', strtotime($detail['tanggal_kembali']))?>)
  	</small>
  </li>
  <li class="list-group-item">
  	<div class="d-flex w-100 justify-content-between">
      <small class="fw-bold">Total Bayar</small>
    </div>
    <small>
  	 	<?=$detail['lama_sewa']?> Hari x <?=number_format($detail['total_harga_sewa'] / $detail['lama_sewa'])?>
	    <span class="fw-bold">
	     | Rp.  <?=number_format($detail['total_harga_sewa'])?>
	    </span>
	  </small>
  </li>
</ul>

<div class="card mt-4">
  <div class="card-header fw-bold">
    <small>Upload Bukti Pembayaran</small>
  </div>
  <div class="card-body">
    <form action="<?=site_url('konfirmasi')?>" method="POST" enctype="multipart/form-data">
			<div class="mb-3">
				<input type="hidden" name="pid" value="<?=$detail['penyewaan_id']?>">
			  <input type="file" name="bukti_transfer" class="form-control" accept="image/*" required>
			</div>

			<div class="d-grid gap-2">
				<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>">
			  <button class="btn btn-secondary btn-sm" type="submit">Kirim Bukti Transfer</button>
			</div>
		</form>
  </div>
</div>

