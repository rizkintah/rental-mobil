<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="<?=base_url('_vendors/plugins/bootstrap/css/bootstrap.min.css')?>">
	 <link rel="stylesheet" href="<?=base_url('_vendors/themes/css/invoice.css?v='.date('is'))?>">
	<link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@200;300;400;500&display=swap" rel="stylesheet">

	<title>Invoice | <?=$this->config->item('apps_title')?> </title>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <div class="container">
	    <a class="navbar-brand" href="#"><?=$this->config->item('apps_title')?></a>
      <ul class="navbar-nav ms-auto">
        <li class="nav-item">
          <a class="nav-link btn btn-info" aria-current="page" href="#" onclick="printJS('printable', 'html')" id="getPDF">Cetak</a>
        </li>
      </ul>
	  </div>
	</nav>

	<div class="container mt-4" id="printable">
		<div class="border-bottom pb-3">
			<div class="header-wrap row d-flex align-items-center">
				<div class="logo-area col-md-10 d-flex justify-content-star align-items-center">
					<img src="<?=base_url('_vendors/themes/images/logo.jpeg')?>" class="img-fluid logo">
					<h5 class="mx-0 ms-2 my-0 fw-light">Trikarya Rent Car</h5>
				</div>
				<div class="col-md-2 text-end">
					<h3 class="m-0">Invoice</h3>
				</div>
			</div>
		</div>

		<!-- <table class="table-header" width="100%" border="0">
			<tr>
				<td width="30%">
					<img src="<?=base_url('_vendors/themes/images/logo.jpeg')?>" class="img-fluid logo" width="80px" style="-webkit-print-color-adjust: exact;">
				</td>
				<td width="30%" align="right">
					<h3>Invoice</h3>
				</td>
			</tr>
		</table> -->

		<table width="50%" border="0" style="font-size: 13px;margin-top: 25px;">
			<tr>
				<td>No. Transaksi</td>
				<td>:</td>
				<td><strong><?=$detail['penyewaan_id']?></strong></td>
			</tr>

			<tr>
				<td>Status Transaksi</td>
				<td>:</td>
				<td><strong><?=getStatusName($detail['status_sewa'])?></strong></td>
			</tr>

			<tr>
				<td>Nama Penyewa</td>
				<td>:</td>
				<td><strong><?=$detail['nama_penyewa']?></strong></td>
			</tr>

			<tr>
				<td>Tanggal Transaksi</td>
				<td>:</td>
				<td><strong><?=date('d M Y', strtotime($detail['tanggal_transaksi']))?></strong></td>
			</tr>
		</table>


		<div class="table-responsive mt-4">
			<table class="table">
				<thead>
					<tr class="text-center">
						<th>Kendaraan</th>
						<th>No. Polisi</th>
						<th>Lama Sewa</th>
						<th>Harga Sewa</th>
						<th>Tanggal Sewa</th>
						<th>Tanggal Kembali</th>
					</tr>
				</thead>
				<tbody>
					<tr class="text-center">
						<td align="center">
							<?=$detail['merk_nama']?> <?=$detail['nama_jenis']?> / <?=$detail['warna']?>
						</td>
						<td align="center">
							<?=$detail['no_polisi']?>
						</td>
						<td align="center">
							<?=$detail['lama_sewa']?> Hari
						</td>
						<td align="center">
							<?=number_format($detail['harga_sewa'])?>
						</td>
						<td align="center">
							<?=date('d M Y', strtotime($detail['tanggal_sewa']))?>
						</td>
						<td align="center">
							<?=date('d M Y', strtotime($detail['tanggal_kembali']))?>
						</td>
					</tr>
				</tbody>
				<tfoot>
					<tr align="right">
						<td colspan="6" align="right" class="fw-bold">
							<strong>Total Bayar: <?=number_format($detail['total_harga_sewa'])?></strong>
						</td>
					</tr>
				</tfoot>
			</table>
		</div>


		<div class="text-information">
			<p>
				Invoice ini sah dan diproses oleh komputer <br>
				Silakan hubungi 
				<a href="https://api.whatsapp.com/send?phone=6283808855891&text=Hai%20admin%2C%20saya%20penyewa%20atas%20nama%3A%20<?=$detail['nama_penyewa']?>,%20dengan%20nomor%20transaksi%3A%0A%20<?=$detail['penyewaan_id']?>" class="text-decoration-none" target="_blank" style="text-decoration: none;">0838-0885-5891</a> 
				apabila kamu membutuhkan bantuan atau pertanyaan.
			</p>	
		</div>

		<style type="text/css">
			@media print {
				* {
					-webkit-print-color-adjust:exact;
				}
				.header-wrap {
					display: flex;
					justify-content: space-between;
					align-items: center;
					border-bottom: 1px solid #eeeeee;
					/*padding-bottom: 3px;*/
				}
				.header-wrap h3 {
					font-size: 30px;
					font-weight: bold;
				}
				.logo-area {
					display: flex;
					justify-content: start;
					align-items: center;
				}
				.logo {
					width: 50px;
				}
				.logo-area h5 {
					margin-left: 10px;
					font-size: 20px;
					font-weight: 400;
				}
				.text-information {
					text-align: center;
					margin-top: 10vh;
				}
				.table {
					font-size: 13px;
					width: 100%;
					border-collapse: collapse;
					color: #212529;
					vertical-align: middle;
					border-color: #dee2e6;
					margin-top: 20px;
					white-space: nowrap;
				}
				.table tbody {
					border-bottom: 1px solid black;	
				}
				.table tfoot {
					border-bottom: 1px solid #eeeeee;	
				}
				.table th {
					background: black;
					color: white;
				}
				.table th, .table td {
				  padding: .5rem;
				}
			
				@page {
            margin-top: 0;
            margin-bottom: 0;
        }
        body {
					font-family: monospace;
					padding-top: 72px;
					padding-bottom: 72px ;
        }
			}
		</style>
	</div>



	<script type="text/javascript" src="https://printjs-4de6.kxcdn.com/print.min.js"></script>


<!-- 
	<script type="text/javascript">

		function printInvoice() {
			var divToPrint=document.getElementById("printable");
        newWin= window.open();
        var style = "<style>";
            style += ".table {width: 100%;font-size: 14px;margin-top:30px}";
            style += ".table, .table th, .table td {border: solid 1px #DDD; border-collapse: collapse;";
            style += "padding: 5px;}";
            style += "h2 {font-size:18px;}";
            style += ".text-information {margin-top:5vh;text-align:center;}";
            style += "</style>";
        newWin.document.write(style)
        newWin.document.write(divToPrint.outerHTML);
        setTimeout(function () { 
                newWin.document.close(); 
                newWin.focus(); // necessary for IE >= 10
                newWin.print();  // change window to winPrint
                newWin.close();// change window to winPrint
        }, 50);
		}
		
	</script> -->
</body>
</html>