	<?php  
		$merkID 	= isset($_GET['merk']) ? $_GET['merk'] : null;
		$jenisID 	= isset($_GET['jenis']) ? $_GET['jenis'] : null;
		$tsewa 		= isset($_GET['tanggal']) ? implode('-', array_reverse(explode('/', $_GET['tanggal']))) : date('Ymd');
		$hari 		= isset($_GET['hari']) ? $_GET['hari'] : 1;
	?>

	<div class="search-area mt-5">
		<form action="<?=current_url()?>" method="GET" id="form-search">
			<div class="row g-3">
			  <div class="col-sm">
			  	<label class="form-label text-white"><small>Merk</small></label>
			    <select name="merk" class="form-control select2 smerk">
			    	<option></option>
			    	<?php foreach ($merkMobil as $k => $v): ?>
			    		<option value="<?=encryptUrl($v['merk_id'])?>" 
			    						<?=$v['merk_id'] == decryptUrl($merkID) ? 'selected' : null;?>>
			    			<?=$v['merk_nama']?>
			    		</option>
			    	<?php endforeach ?>
			    </select>
			  </div>

			  <div class="col-sm">
			  	<label class="form-label text-white"><small>Tipe Mobil</small></label>
			    <input type="hidden" name="jenis" value="<?=$jenisID?>">
			    <select class="form-control" id="ijenis">
			    	<option value="">Pilih Tipe Mobil</option>
			    </select>
			  </div>

			  <div class="col-sm">
			  	<label class="form-label text-white"><small>Tanggal Sewa</small></label>
			  	<div class="input-group date" id="datetimepicker2" data-target-input="nearest">
					  <input type="text" name="tanggal" class="form-control datetimepicker-input" data-target="#datetimepicker2" placeholder="dd/mm/yyyy" data-toggle="datetimepicker" value="<?=date('d/m/Y', strtotime($tsewa))?>">
					  <button class="btn btn-light" type="button" data-target="#datetimepicker2" data-toggle="datetimepicker">
					  	<i class="fas fa-calendar-alt"></i>
					  </button>
					</div>
			  </div>

			  <div class="col-sm-2">
			  	<label class="form-label text-white"><small>Lama Sewa</small></label>
			  	<input name="hari" type="number" data-suffix="Hari" value="<?=$hari?>" min="1" max="20" step="1" class="spinner-input" />
			  </div>

			  <div class="col-sm-2 d-grid gap-1">
			  	<label class="visually-hidden form-label text-white">Search</label><br>
			  	<button type="submit" class="btn btn-warning shadow-sm">Cari</button>
			  	<!-- <input type="submit" name="btnsearch" value="Cari" class="btn btn-warning shadow-sm"> -->
			  </div>
			</div>
		</form>
	</div>
	
	<div class="row lists-area mt-4"></div>

	

