<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

	private $tPenyewaan 			= 'penyewaan';
	private $vMobil 					= 'v_mobil';
	private $vMobilThumbail 	= 'v_mobil_thumbnail';
	private $tPengguna 				= 'pengguna';

	function getListPenyewaanBy($statusPenyewaan = null) {
		$userID = $this->session->userdata('userID');
		$whereSewa = "AND status_sewa IN (1,4,5,6, 7)";

		if(!empty($statusPenyewaan)) $whereSewa = "AND status_sewa = '{$statusPenyewaan}'";

		$sql = "SELECT penyewaan.*, mobil.jenis_id, mobil.nama_jenis, 
							   mobil.merk_id, mobil.merk_nama, mobil.harga_sewa, thumb.image_file 
						FROM {$this->tPenyewaan} penyewaan
						JOIN {$this->vMobil} mobil 
						ON penyewaan.mobil_id = mobil.mobil_id
						LEFT OUTER JOIN {$this->vMobilThumbail} thumb 
						ON mobil.mobil_id = thumb.mobil_id
						WHERE penyewaan.pengguna_id = '{$userID}'
						{$whereSewa}
						ORDER BY penyewaan.tanggal_transaksi DESC";

		$data = $this->db->query($sql)->result_array();
		return $data;
	}

	function getDetailPenyewaanBy($penyewaanID) {
		$this->db->select('penyewaan.*, mobil.nama_jenis, mobil.merk_nama', false);
		$this->db->from("{$this->tPenyewaan} penyewaan");
		$this->db->join("{$this->vMobil} mobil", 'penyewaan.mobil_id = mobil.mobil_id');
		$this->db->where('penyewaan.penyewaan_id', $penyewaanID);
		$q = $this->db->get()->row_array();
		return $q;
	}

	function updateKonfirmasiPembayaran($penyewaanID, $imgFile) {
		$arr = array(
			'status_sewa' => '7',
			'bukti_pembayaran' => $imgFile
		);

		$this->db->where('penyewaan_id', $penyewaanID);
		$q = $this->db->update($this->tPenyewaan, $arr);
		return $q;
		
	}

	function getDetailInvoice($noInvoice)
	{
		$this->db->select('A.warna, A.no_polisi, A.nama_jenis, A.merk_nama, A.image_file, A.harga_sewa,
	   									 B.penyewaan_id, B.lama_sewa, B.total_harga_sewa, B.tanggal_sewa, 
	   									 B.tanggal_kembali, B.status_sewa, B.tanggal_transaksi, C.nama_pengguna nama_penyewa');
		$this->db->from("{$this->vMobil} A");
		$this->db->join("{$this->tPenyewaan} B", 'A.mobil_id = B.mobil_id');
		$this->db->join("{$this->tPengguna} C", 'B.pengguna_id = C.pengguna_id');
		$this->db->where('B.penyewaan_id', $noInvoice);
		return $this->db->get()->row_array();
	}

}

/* End of file Dashboard_model.php */
/* Location: ./application/modules/frontend/models/Dashboard_model.php */