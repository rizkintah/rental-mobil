<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend_model extends CI_Model {

	private $tMobil 				= 'mobil';
	private $vMobil 				= 'v_mobil';
	private $vMobilThumbail = 'v_mobil_thumbnail';
	private $tMobilImage 		= 'mobil_images';
	private $tPenyewaan 		= 'penyewaan';
	

	function getListMobil($merkID, $jenisID, $tglSewa, $JmlHari)
	{

		/**
		 * Query yg pertama utk mengambil semua mobil yg tersedia (tdk ada yg menyewa) 
		 * Query yg kedua utk mengambil data dr penyewaan yg range nya bukan pd periode yg,
		 * sudah ada pada penyewaan
		 * */
		if(!empty($merkID)) $merkID = "AND merk_id = '{$merkID}'";
		if(!empty($jenisID)) $jenisID = "AND jenis_id = '{$jenisID}'";

		$sql = "SELECT A.*
						FROM {$this->vMobil} A
						WHERE status = 'tersedia'
						UNION
						SELECT A.*
						FROM {$this->vMobil} A, {$this->tPenyewaan} B
						WHERE A.mobil_id = B.mobil_id
						AND status_sewa <> 2
						AND A.mobil_id NOT IN (
							SELECT mobil_id
							FROM {$this->tPenyewaan}
							WHERE status_sewa <> 2
							AND  tanggal_sewa  BETWEEN '{$tglSewa}' AND DATE_ADD('{$tglSewa}', INTERVAL {$JmlHari} DAY)
							AND tanggal_kembali  BETWEEN '{$tglSewa}' AND DATE_ADD('{$tglSewa}', INTERVAL {$JmlHari} DAY)
						);
						{$merkID}
						{$jenisID}";

		$data = $this->db->query($sql);
		return $data->result_array();
	}

	function getListMobilImage($mobilID)
	{
		$this->db->where('mobil_id', $mobilID);
		return $this->db->get($this->tMobilImage)->result_array();
	}

	function insertPenyewaan($data) {
		$insertPenyewaan = $this->db->insert($this->tPenyewaan, $data);
		if ($insertPenyewaan) {
			$this->db->where('mobil_id', $data['mobil_id']);
			$this->db->update($this->tMobil, array('status' => 'tidak tersedia'));
		}
		return false;
	}

}

/* End of file Frontend_model.php */
/* Location: ./application/modules/frontend/models/Frontend_model.php */